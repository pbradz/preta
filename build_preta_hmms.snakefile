# Trim alignments and build HMMs:

rule preTA_hmms:
    input: expand("data/processed/hmm/{g}-trimmed-automated1.hmm", g=GENES)

rule preTA_alns:
    input: "data/raw/{g}.fa"
    output: "data/processed/aln/{g}.fasta_aln"
    threads: 1
    shell: """
    t_coffee -in={input} -mode=regular -output=score_html \
            clustalw_aln fasta_aln score_ascii phylip -maxnseq=150 \
            -maxlen=10000 -case=upper -seqnos=off -outorder=input  \
            -run_name={output} -n_core=1 -quiet=stdout
    """


rule trim_alns:
#    input: "data/manual/{g}-all-human-tcoffee.clustalw"
    input: "data/processed/aln/{g}.fasta_aln"
    output: "data/processed/aln/{g}-trimmed-automated1.aln"
    shell: "trimal -in {input} -out {output} -automated1"

rule build_hmms:
    input: "data/processed/aln/{g}-trimmed-automated1.aln"
    output: "data/processed/hmm/{g}-trimmed-automated1.hmm"
    shell: "hmmbuild {output} {input}"

rule concat_hmms:
    input: expand("data/processed/hmm/{g}-trimmed-automated1.hmm", g=GENES)
    output: "data/processed/hmm/preTA-trimmed-automated1.hmm"
    shell: "cat {input} > {output}"

