import os
import glob
import tempfile
import re

configfile: "config.json"
GENES = config["genes"]
GFFTYPES = config["rungffs"]
wildcard_constraints:
    marker="|".join(GENES),
    pair="\d"

include: "download.snakefile"
include: "build_preta_hmms.snakefile"
include: "scan_genomes.snakefile"
include: "annotate_genome_results.snakefile"
include: "scan_metagenomes.snakefile"
