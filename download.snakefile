rule download_midas_1:
   output: "data/raw/midas_db_v1.0.tar.gz"
   shell: "cd data/raw/ && wget http://lighthouse.ucsf.edu/MIDAS/midas_db_v1.0.tar.gz"

rule unzip_midas_1:
   input: "data/raw/midas_db_v1.0.tar.gz"
   output: directory("data/raw/midas/midas_db_v1.0/")
   shell: "cd data/raw/ && mkdir -p midas && cd midas && tar xvfz ../`basename {input}`"

rule get_ncbi_preA:
    output: "data/raw/preA.fa"
    shell: """
    esearch -db protein -query \
            "ARQ46501.1 OR \
             NP_000101.2 OR \
             EDU36025.1 OR \
             EDR96331.1 OR \
             AUX97234.1 OR \
             AVI56643.1" | \
             efetch -format fasta > {output}
    """

rule get_ncbi_preT:
    output: "data/raw/preT.fa"
    shell: """
    esearch -db protein -query \
            "ARQ46500.1 OR \
             NP_000101.2 OR \
             EDU36024.1 OR \
             EDR96330.1 OR \
             AUX97235.1 OR \
             AVI56642.1" | \
             efetch -format fasta > {output}
    """
