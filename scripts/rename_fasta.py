#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
@click.option('--keep/--no-keep', '-k', default=False)
def process(infile, outfile, keep):
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      d = record.description
      #r = re.sub(r'(GCF_.*)\.cds_.*\!\!lcl\|.*prot_(??_.[^_]*).*', 
      g = re.match(r'(GCF_.*)\.cds_.*\!\!.*', 
                   d)
      if g:
        g0 = g.group(1)
      else:
        g0 = "None"
      p = re.match(r'.*\!\!lcl\|.*prot_(.._[^_]*).*', 
                   d)
      if p:
        p0 = p.group(1)
      else:
        p = re.match(r'.*\!\!lcl\|.*prot_([^_]*).*',
            d)
        if p:
          p0 = p.group(1)
        else:
          p0 = "None"
      r = copy.deepcopy(record)
      r.name = g0
      if keep:
        r.description = p0
      else:
        r.description = ''
      r.id = g0
      SeqIO.write(r, oh, "fasta")
  oh.close()    

if __name__ == '__main__':
  process()
