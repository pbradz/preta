#!/usr/bin/env python3
import os, sys, click
from math import ceil, floor, log10

@click.command()
@click.option('--ndirs', default=100, type=int, help='Number of directories')
@click.option('--layers', default=1, type=int, help='Number of layers deep')
@click.option('--nfiles', default=None, type=int,
    help='Number of files per directory (will override ndirs)')
@click.option('--reorder', default=True, type=bool,
    help='Directories will be ordered alphabetically')
@click.option('--loop', default=False, type=bool,
    help='Use a for loop instead of modulo (single layer only)')
def Run(ndirs, layers, nfiles, reorder, loop):
    lines = [line for line in sys.stdin]
    nl = len(lines)
    if nfiles is not None:
        ndirs = round(nl / float(nfiles))
        sys.stderr.write("(Using %d directories...)\n" % ndirs)
    dirsperlayer = round(ndirs ** (1/float(layers)))
    factor = [1/(dirsperlayer ** l) for l in range(layers)]
    dirs = [[((x * y) % dirsperlayer) for x in factor] for y in range(nl)]
    padding = ceil(log10(dirsperlayer))
    if loop:
        count = 0
        filesperlayer = nl / ndirs
        for (n, l) in enumerate(lines):
            if (floor(n % filesperlayer) == 0): count += 1
            sys.stdout.write("{:0{pad}d}\t{}".format(count, l, pad=padding))
        return
    dirstrings = [os.path.join(*["{:0{pad}d}".format(round(i) + 1, pad=padding)
      for i in reversed(d)]) for d in dirs]
    if reorder:
      dirstrings.sort()
    for (n, line) in enumerate(lines):
        sys.stdout.write("{}\t{}".format(dirstrings[n], line)) 

if __name__ == "__main__":
    Run()
