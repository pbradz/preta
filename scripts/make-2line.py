#!/usr/bin/env python

import sys
from Bio import SeqIO

for record in SeqIO.parse(sys.stdin, "fasta"):
  SeqIO.write(record, sys.stdout, "fasta-2line")

