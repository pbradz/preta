#!/usr/bin/python

import os
import sys
import argparse

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import gzip
import operator
import re

p = argparse.ArgumentParser(description = \
    "rename PATRIC 16S records for DADA2 based on MIDAS db")
p.add_argument("fafile", type = str,  help = "input file")
p.add_argument("midasfile", type = str, help = "MIDAS genome_info.txt file")
p.add_argument("-z", dest="compressed", default = False, action = "store_true", \
    help = "uncompress file (gz)")
p.add_argument("-s", dest="suppress", default = False, action = "store_true", \
    help = "suppress entries that don't map to a MIDAS id")
args = p.parse_args()

midas_id = dict()
with open(args.midasfile, 'rt') as fh:
  header = fh.readline()
  for line in fh:
    row = line[:-1].split("\t")
    midas_id[row[0]] = row[5]

if (args.compressed):
  fh = gzip.open(args.fafile, "rt")
else:
  fh = open(args.fafile, "rt")
nrec = 0
midas_hit = set()
for record in SeqIO.parse(fh, "fasta"):
  desc = record.description
  tag = re.search("\[(.*)\]", record.description).group(0)
  full_name = re.sub("\[(.*) \| .*$", "\\1", tag)
  patric_genome = re.sub("\[.* \| (.*)]$", "\\1", tag)
  genus_species = full_name.split(" ")[0:2]
  midas_present = midas_id.has_key(patric_genome)
  midas_species = midas_id[patric_genome] if midas_present else genus_species[1]
  record.description = " ".join([genus_species[0], midas_species])
  if midas_present or (not args.suppress):
    SeqIO.write(record, sys.stdout, "fasta")
    nrec += 1
    if midas_present: midas_hit.add(midas_id[patric_genome])
fh.close()
sys.stderr.write("%d records mapping to %d MIDAS ids written\n" % (nrec, \
  len(midas_hit)))
