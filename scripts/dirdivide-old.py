#!/usr/bin/env python3
import os, sys
from math import ceil, floor, log10

ndirs = int(sys.argv[1])
padding = ceil(log10(ndirs))
lines = [line for line in sys.stdin]
nl = len(lines)
filesperdir = (nl / float(ndirs))
count = 0
for (n, line) in enumerate(lines):
    if (floor(n % filesperdir) == 0):
        count += 1
    sys.stdout.write("{:0{pad}d}\t{b}".format(count, pad=padding, b=line))
