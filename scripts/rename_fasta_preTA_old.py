#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
@click.option('--keep/--no-keep', '-k', default=False, is_flag=True)
@click.option('--protein/--genome', '-p', default=False, is_flag=True)
@click.option('--gcf-std', '-g', default=False, is_flag=True)
@click.option('--filter-file', '-F', type=str, default=None)
@click.option('--dedupe', '-d', is_flag=True, default=False)
def process(infile, outfile, keep, protein, gcf_std, filter_file, dedupe):
  if dedupe:
    Found = set()
  if filter_file:
    FilterSet = set()
    with open(filter_file, 'r') as fh:
      for line in fh:
        FilterSet.add(line[:-1])
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      d = record.description
      g0 = ''
      #r = re.sub(r'(GCF_.*)\.cds_.*\!\!lcl\|.*prot_(??_.[^_]*).*', 
      g = re.match(r'.*(GCF_.*)\.cds_?.*\!\!.*', 
                   d)
      if g:
        g0 = g.group(1)
      else:
        g = re.match(r'.*(GCF_.*)\.faa_?.*\!\!.*', 
                     d)
        if g:
          g0 = g.group(1)
        else:
          g0 = "None"
      p = re.match(r'.*\!\!lcl\|.*prot_(.._[^_]*).*', 
                   d)
      if p:
        p0 = p.group(1)
      else:
        p = re.match(r'.*\!\!lcl\|.*prot_([^_]*).*',
            d)
        if p:
          p0 = p.group(1)
        else:
          p = re.match(r'.*\!\!([^ ]*).*\!\!evalue.*',
              d)
          if p:
            p0 = p.group(1)
          else:
            p0 = "None"
      r = copy.deepcopy(record)
      if gcf_std:
        a = g0.split("_")
        if (len(a) > 1):
          g0 = "_".join(a[0:2])
      if protein:
        r.name = p0
        r.id = p0
        if keep:
          r.description = g0
        else:
          r.description = ''
      else:
        r.name = g0
        r.id = g0
        if keep:
          r.description = p0
        else:
          r.description = ''
      if dedupe:
        if r.name in Found:
          sys.stderr.write("skipping subsequent occurrence of %s\n" % r.name)
          continue
        else:
          Found.add(r.name)
      if filter_file:
        if not (r.name in FilterSet):
          continue
      SeqIO.write(r, oh, "fasta")
  oh.close()    

if __name__ == '__main__':
  process()
