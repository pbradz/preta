#!/usr/bin/env python

import os, sys, click, glob, subprocess, re
from joblib import Parallel, delayed

def RunHMM(ngff, gff, sd, wildcard, outputDir, nseqs, unit, totalgffs, preA,
        preT, log, debug, swapout): 
    if (ngff % unit) == 0:
        print(F"{ngff} out of {totalgffs}...")
    gffname = os.path.basename(gff)
    gffdir = os.path.dirname(gff)
    if debug: print(F"GFFs in {gffdir}")
    odir = os.path.join(outputDir, sd)
    genomename = re.sub(swapout, '', gffname)
    if debug: print(f" ... looking for {genomename}{wildcard}")
    genomefiles = glob.glob(os.path.join(gffdir, F"{genomename}{wildcard}"))
    if debug: print(f" ... found {len(genomefiles)} files")
    for (n, genomefile) in enumerate(genomefiles):
        for (g, hmm) in zip(["preA", "preT"], [preA, preT]):
            hmmcmd = ["hmmsearch", 
                    "-Z",
                    F"{nseqs}",
                    "--tblout",
                    F"{odir}/{genomename}_{g}_{n}_output.tab",
                    F"{hmm}",
                    F"{genomefile}"]
            if debug: print(" ".join(hmmcmd))
            if log:
                outfn = F"{odir}/log/{genomename}_{g}_{n}.log"
                with open(outfn, 'w') as fh:
                    subprocess.run(hmmcmd, stdout=fh)
            else:
                subprocess.run(hmmcmd, stdout=subprocess.DEVNULL)

@click.option('--wildcard', help='glob wildcard')
@click.option('--gfflist', help='file with list of subdirectories and gffs')
@click.option('--gdir', help='gff dir')
@click.option('--nseqfile', help='file w/ number of sequences')
@click.option('--outdir', help='output (root) directory')
@click.option('--preT', 'preT', help='preT HMM')
@click.option('--preA', 'preA',  help='preA HMM')
@click.option('--threads', type=int, default=6, help='number of threads')
@click.option('--log', type=bool, default=True, help='log results')
@click.option('--debug', type=bool, default=False, help='print debugging info')
@click.option('--swapout', default='\.gff', help='string to swap out and replace with wildcard')
@click.command()
def Run(wildcard, gfflist, gdir, nseqfile, outdir, preT, preA, threads, log,
        debug, swapout):
    with open(gfflist, 'r') as fh:
        # idiom to return two separate lists from tab-delimited rows
        [subdir, gffs] = map(list, zip(*[line[:-1].split('\t') for line in fh]))
    with open(nseqfile, 'r') as fh:
        nseqs = int(fh.readline())
    totalgffs = len(gffs)
    unit = int(totalgffs/500.0)
    sys.stderr.write("Making subdirectories (may take a while)...\n")
    for sd in subdir:
        os.makedirs(os.path.join(outdir, sd), exist_ok=True)
        if log: 
            os.makedirs(os.path.join(outdir, sd, "log"), exist_ok=True)
    sys.stderr.write("Running HMMs...\n")
    Parallel(n_jobs=threads)(delayed(RunHMM)(ngff=ngff, gff=gff,
        sd=subdir[ngff], wildcard=wildcard, outputDir=outdir, nseqs=nseqs,
        unit=unit, totalgffs=totalgffs, preA=preA, preT=preT, log=log,
        debug=debug, swapout=swapout) for (ngff, gff)
        in enumerate(gffs))

if __name__ == "__main__":
    Run()
    print(gffname)
