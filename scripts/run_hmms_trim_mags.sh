#!/bin/bash

PRETA_ROOT=$HOME/projects/preta/

for x in `cat mags-faa.txt`; do
  echo $x
	b=`basename $x .faa`
	echo ... $b
	hmmsearch -Z 310557465 --tblout $PRETA_ROOT/data/hmm-output/trim_mag_output/${b}_preA_${count}_output.tab $PRETA_ROOT/sequences/preA-trimmed-automated1.hmm $x > $PRETA_ROOT/data/hmm-output/logs/${b}_preA_trimmed_auto1.log
	hmmsearch -Z 310557465 --tblout $PRETA_ROOT/data/hmm-output/trim_mag_output/${b}_preT_${count}_output.tab $PRETA_ROOT/sequences/preT-trimmed-automated1.hmm $x > $PRETA_ROOT/data/hmm-output/logs/${b}_preT_trimmed_auto1.log
done
