#!/usr/bin/python

import sys, os
import csv
import re
import glob
from Bio import SearchIO
#from ete2 import NCBITaxa

EThresh = 1e-2
# GenomeFile = "/pollard/home/pbradz/projects/preta/scripts/euk_arc_genomes.txt"
#NamesFile = "/pollard/home/pbradz/data/ncbi/names.dmp"
#TabsDir = "/pollard/home/pbradz/projects/preta/data/parsed-gffs/tabs"
ResultDir = sys.argv[1]
GenomeFile = sys.argv[2] # "/pollard/home/pbradz/projects/preta/scripts/euk_arc_genomes.txt"
RefSeqFile = sys.argv[3] 

def CommentStripper(iterator):
  for line in iterator:
    if line.startswith('#'):
      continue
    if not line.strip():
      continue
    yield line

class Hit:
  def __init__(self, pid, e_val, desc):
    self.pid = pid
    self.e_val = e_val
    self.desc = desc

def HMMParse(filename):
  results = SearchIO.read(filename, "hmmer3-tab")
  hitlist = [Hit(r.id, float(r.evalue), r.description) for r in results]
  hits = dict(zip([h.pid for h in hitlist], hitlist))
    #parsed = re.findall("(\[[^\]]*=[^\]]*\])", desc)
    #for p in parsed:
    #  m = re.match("\[(.*)=(.*)\]", p)
    #  if not m == None:
    #    if (m.group(1) == "protein_id"):
    #      hits[m.group(2)] = e_val
  return(hits)

def NCBIParse(filename):
  taxa = dict()
  with open(filename, 'r') as fh:
    lastorg = "NA" 
    for line in fh:
      rem_end_delim = re.sub("\t\|\n", "", line)
      row = rem_end_delim.split("\t|\t")
      org = str(row[0])
      if row[3] == "scientific name":
        taxa[org] = row[1]
      if (not (org == lastorg)) and \
          (not(lastorg == "NA")) and \
          (not taxa.has_key(lastorg)):
        taxa[lastorg] = "node #%s" % org
      lastorg = org
  return(taxa)

def GFFParse(filename):
  proteins = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      if not row["protein_id"] == "None":
        pid = row["protein_id"]
        proteins[pid] = dict()
        proteins[pid]["taxon"] = row["taxon"]
        proteins[pid]["gene_number"] = int(row["gene_number"])
        proteins[pid]["strand"] = row["strand"]
  return(proteins)

def oldTaxonomyParse(filename, taxonomy):
  species_to_midas = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      gid = str(row["taxon_id"])
      species = MakeSpecies(gid, taxonomy)
#      lineage = taxonomy.get_lineage(gid)
#      ranks = taxonomy.get_rank(gid)
#      species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
#          if i == "species"]
#      print >> sys.stderr, "%s == %s == %s" % (gid, species, row["species_id"])
      if (len(species) == 1):
        species_to_midas[str(species[0])] = str(row["species_id"])
  return(species_to_midas)

def TaxonomyParse(filename):
  species_to_midas = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      species_to_midas[row["species_name"]] = str(row["species_id"])
  return(species_to_midas)

def SpeciesInfoParse(filename):
  midas_names = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      midas_names[row["species_id"]] = row["species_name"]
  return(midas_names)

def PhenotypeParse(filename):
  phenotype = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      phenotype[row["ID"]] = float(row["logit.Prevalence"])
  return(phenotype)

def MakeSpecies(taxon, taxonomy):
  lineage = taxonomy.get_lineage(taxon)
  ranks = taxonomy.get_rank(lineage)
  species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
      if i == "species"]
  return(species)


# read genomes file
print("loading genome list", file=sys.stderr)
with open(GenomeFile, "r") as fh:
  genomes = [line[:-1] for line in fh]


print("loading refseq references", file=sys.stderr)
with open(RefSeqFile, "r") as fh:
  header = fh.readline()
  oname = dict(zip(genomes, ['NA' for g in genomes]))
  csv_reader = csv.DictReader(fh, delimiter='\t')
  for (ln, row) in enumerate(csv_reader):
    if (ln > 1):
      for g in genomes:
        if g == (row["ftp_path"].split("/"))[-1]:
          oname[g] = row["organism_name"]

print("\t".join(["Genome",
  "Genome name",
  "preA protein ID",
  "preT protein ID",
  "preA eval",
  "preT eval",
  "desc"]))
for g in genomes:
  try:
    a_hits = HMMParse(os.path.join(ResultDir, "%s_preA_0_output.tab" % g))
    t_hits = HMMParse(os.path.join(ResultDir, "%s_preT_0_output.tab" % g))
  except ValueError:
    print("Skipping %s (no results for either PreA or PreT)" % g,
        file=sys.stderr)
    continue
  except IOError:
    print("Skipping %s (result file not found)" % g,
        file=sys.stderr)
    continue
  a_hits_passed = set([a for a in a_hits.keys() if a_hits[a].e_val < EThresh])
  t_hits_passed = set([t for t in t_hits.keys() if t_hits[t].e_val < EThresh])
  both_hits_passed = a_hits_passed.intersection(t_hits_passed)
  if len(both_hits_passed) == 0:
    print("No preTA hits in genome %s" % g, file=sys.stderr)
  for b in both_hits_passed:
    print("\t".join([
      ("%s" % g),
      ("%s" % oname[g]),
      ("%s" % a_hits[b].pid),
      ("%s" % t_hits[b].pid),
      ("%s" % a_hits[b].e_val),
      ("%s" % t_hits[b].e_val),
      ("%s" % a_hits[b].desc)
      ]))

