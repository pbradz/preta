#!/usr/bin/env python

import os
import sys
import click
import pandas as pd
from Bio import SeqIO
import numpy as np
from scipy import stats
from bashplotlib.histogram import plot_hist

@click.command()
@click.option('--fasta', '-f', type=str)
@click.option('--q-trim', '-q', type=float, default=0.2)
@click.option('--sd-trim', '-s', type=float, default=2.0)
@click.option('--hist/--no-hist', '-h/-H', default=False)
@click.option('--display/--no-display', '-d/-D', default=True)
@click.option('--print-extremes/--no-print-extremes', '-x/-X', default=False)
def run(fasta, hist, display, print_extremes, sd_trim, q_trim):
    with open(fasta, 'r') as fh:
        records = [r for r in SeqIO.parse(fh, 'fasta')]
    lengths = [len(r.seq) for r in records]
    if hist:
        plot_hist(lengths, xlab=True, bincount=50, height=12)
    if print_extremes:
        records.sort(key=lambda r: len(r.seq))
        SeqIO.write(records[0], sys.stdout, "fasta")
        SeqIO.write(records[-1], sys.stdout, "fasta")
    llengths = np.log(lengths)
##   old mode code
#    dens = stats.gaussian_kde(llengths)
#    xs = np.linspace(min(llengths), max(llengths), 500)
#    ys = dens.evaluate(xs)
#    lmode = np.mean(xs[ys == max(ys)])
#    mode = np.exp(lmode)
    lmean = stats.tmean(llengths,
                        limits=(np.quantile(llengths, q_trim/2),
                                np.quantile(llengths, 1-(q_trim/2))))
    lsd = stats.tstd(llengths,
                     limits=(np.quantile(llengths, q_trim/2),
                             np.quantile(llengths, 1-(q_trim/2))))
    lbtm = lmean - (lsd * sd_trim)
    ltop = lmean + (lsd * sd_trim)
    btm = np.exp(lbtm)
    top = np.exp(ltop)
    sys.stderr.write("Keeping sequences between %f and %f letters long\n" % (btm, top))
    new_r = [r for r in records if btm <= len(r.seq) <= top]
    sys.stderr.write("%d sequences retained out of %d\n" % (len(new_r), len(records)))
    if display:
        for r in new_r:
            SeqIO.write(r, sys.stdout, "fasta")
    if print_extremes:
        new_r.sort(key=lambda r: len(r.seq))
        SeqIO.write(new_r[0], sys.stdout, "fasta")
        SeqIO.write(new_r[-1], sys.stdout, "fasta")
    if hist:
        new_lengths = [len(r.seq) for r in new_r]
        plot_hist(new_lengths, xlab=True, bincount=50, height=12)


if __name__ == '__main__':
  run()
