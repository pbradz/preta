#!/bin/bash
# run on chestnut

for x in `cat genomes.txt`; do
	y=("/pollard/home/jshi/db/refseq/complete_lastest_bact/all_records/$x*");
  z=("${y[0]}/*translated_cds*gz")
  echo $x
	count=0
	for Z in $z; do
		echo ... $Z
		hmmsearch -Z 33383393 --tblout output/${x}_preA_${count}_output.tab ~/projects/preta/sequences/preA-all-switch.hmm $Z > logs/${x}_preA.log
		hmmsearch -Z 33383393 --tblout output/${x}_preT_${count}_output.tab ~/projects/preta/sequences/preT-all-switch.hmm $Z > logs/${x}_preT.log
		(( count++ ))
	done
done
