#!/usr/bin/env python

import sys, statistics, os, re

filename = sys.argv[1]
title = re.sub("_cds_from_genomic.matches.txt", "", os.path.basename(filename))
match_dict = dict()
n_total_hits = 0
with open(filename, 'r') as fh:
    for line in fh:
        n_total_hits += 1
        row = line[:-1].split('\t')
        matches = row[1].split('_')
        species = matches[0]
        marker = matches[3]
        pctid = float(row[2])
        if (species in match_dict.keys()):
            match_dict[species].append(pctid)
        else:
            match_dict[species] = [pctid]
if (len(match_dict) > 0):
  sorted_sp = sorted(match_dict.items(),
      key=lambda x: (-len(x[1]), -statistics.median(x[1])))
  top_species = sorted_sp[0][0]
  sys.stdout.write("{}\t{}\t{}\t{}\t{}\n".format(title,
    top_species,
    len(match_dict[top_species]),
    statistics.median(match_dict[top_species]),
    n_total_hits))
else:
  sys.stdout.write("{}\tNA\t0\t0\t0\n".format(title))
