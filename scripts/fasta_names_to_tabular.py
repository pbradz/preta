#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
def process(infile, outfile):
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  oh.write("{}\t{}\t{}\t{}\n".format("genome", "pid", "e_val", "full_desc"))
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      i = record.id
      d = " ".join(record.description.split(" ")[1:])
      dr = d.split("!!")
      genome = re.sub(".faa$", "", re.sub(".cds$", "", dr[0]))
      pid_raw = dr[1].split(" ")[0] 
      p = re.match(r'.*\[protein_id=([^\]]*)].*', dr[1])
      if p:
        pid = p.group(1)
      else:
        pid = pid_raw
      ev = re.match(r'evalue: (.*)', dr[2])
      if ev:
        e_val = float(ev.group(1))
      else:
        e_val = None
      oh.write("{}\t{}\t{}\t{}\n".format(genome, pid, e_val, d))
  oh.close()    

if __name__ == '__main__':
  process()
