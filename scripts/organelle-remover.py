#!/usr/bin/env python

import click
import sys
import re
from Bio import SeqIO


@click.command()
@click.argument('filename')
@click.option('--exclude', '-x', multiple=True)
def run(filename, exclude):
  records = []
  with open(filename, 'rU') as handle:
    for record in SeqIO.parse(handle, 'fasta'):
      d = record.description
      found = any([re.search(x, d) is not None for x in exclude])
      if not found:
        records.append(record)
  SeqIO.write(records, sys.stdout, "fasta")        
 
if __name__ == "__main__":
  run()
