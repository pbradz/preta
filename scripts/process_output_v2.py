#!/usr/bin/env python

import sys, os
import csv
import re
import glob
import click
import ast
from Bio import SearchIO
from ete3 import NCBITaxa

def CommentStripper(iterator):
  for line in iterator:
    if line.startswith('#'):
      continue
    if not line.strip():
      continue
    yield line

class Hit:
  def __init__(self, pid, e_val, desc):
    self.pid = pid
    self.e_val = e_val
    self.desc = desc

def PID(pid):
  pidR = re.sub(".*prot_([^_]*)_([^_]*)_.*", "\\1_\\2", pid)
  return(pidR)

def AnnotID(desc):
  pid = re.sub(".*ID=([^;]*);?.*", "\\1", desc)
  return(pid)

def HMMParse(filename, pidregex, mags=False):
  results = SearchIO.read(filename, "hmmer3-tab")
  if mags:
    hitlist = [Hit(AnnotID(r.description), float(r.evalue), r.description) for r in results]
  else:
    hitlist = [Hit(r.id, float(r.evalue), r.description) for r in results]
  if pidregex:
    hits = dict(zip([PID(h.pid) for h in hitlist], hitlist))
  else:
    hits = dict(zip([h.pid for h in hitlist], hitlist))
  return(hits)

def NCBIParse(filename):
  taxa = dict()
  with open(filename, 'r') as fh:
    lastorg = "NA"
    for line in fh:
      rem_end_delim = re.sub("\t\|\n", "", line)
      row = rem_end_delim.split("\t|\t")
      org = str(row[0])
      if row[3] == "scientific name":
        taxa[org] = row[1]
      if (not (org == lastorg)) and \
          (not(lastorg == "NA")) and \
          (not lastorg in taxa):
        taxa[lastorg] = "node #%s" % org
      lastorg = org
  return(taxa)

def GFFParse(filename, mags=False):
  proteins = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      if not row["protein_id"] == "NA":
        if mags:
          pids = ast.literal_eval(row["protein_id"])
        else:
          pids = row["protein_id"].split(",")
        for pid in pids:
          proteins[pid] = dict()
          proteins[pid]["taxon"] = row["taxon"]
          proteins[pid]["feature_number"] = int(row["feature_number"])
          proteins[pid]["strand"] = row["strand"]
  return(proteins)

def oldTaxonomyParse(filename, taxonomy):
  species_to_midas = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      gid = str(row["taxon_id"])
      try:
        species = MakeSpecies(gid, taxonomy)
        if (len(species) == 1):
          species_to_midas[str(species[0])] = str(row["species_id"])
      except ValueError:
        print("taxon id %s not found, skipping" % gid, file=sys.stderr)
  return(species_to_midas)

def TaxonomyParse(filename):
  species_to_midas = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      species_to_midas[row["species_name"]] = str(row["species_id"])
  return(species_to_midas)

def SpeciesInfoParse(filename):
  midas_names = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      midas_names[row["species_id"]] = row["species_name"]
  return(midas_names)

def PhenotypeParse(filename):
  phenotype = dict()
  with open(filename, 'r') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      phenotype[row["ID"]] = float(row["logit.Prevalence"])
  return(phenotype)

def MakeSpecies(taxon, taxonomy):
  lineage = taxonomy.get_lineage(taxon)
  ranks = taxonomy.get_rank(lineage)
  species = [list(ranks.keys())[n] for (n, i) in enumerate(list(ranks.values())) \
      if i == "species"]
  return(species)

@click.command()
@click.option('--resultdir', default='./results/',
    help='Directory in which to look for HMM results')
@click.option('--neighborhood', default=1,
    help='Classify preT/preA pairs as an operon if they are at least this'
    ' close')
@click.option('--filterexact', default=False,
    help='Filter out DPYDs (i.e., where preT and preA hit the same gene)')
@click.option('--nofilter', default=False,
    help='Return annotations for all preTs and preAs above threshold '
    'regardless of whether or not they are in a preTA operon')
@click.option('--pidregex', default=False,
    help='parse out name of protein ID from HMM hits')
@click.option('--ncbi-annot/--no-ncbi-annot', default=True,
    help='Whether to attempt to give taxonomic annotations')
@click.option('--ethresh', default=1e-2,
    help='Set the minimum e-value threshold for reporting HMM hits')
@click.option('--genomefile', default='data/processed/parsed-gffs/euk_arc_genomes.txt',
    help='File containing paths to genomes')
@click.option('--namesfile',
    default='data/raw/names.dmp',
    help='File containing names.dmp from NCBI')
@click.option('--tabsdir',
    default='data/processed/parsed-gffs/tabs',
    help='File containing tabular results of parsing GFFs')
@click.option('--datadir',
    default='data/raw/',
    help='Directory where species info, taxonomy file, and '
    'prevalence files are to be found')
@click.option('--divided', default=False,
    help='Walk down subdirectories of result dir')
@click.option('--divmapfile', default='',
    help='File containing mapping of result subdirectories to genomes')
@click.option('--mags/--no-mags', default=False,
    help='Whether results were run on MAGs from IGGdb')
def run(resultdir,
        neighborhood,
        filterexact,
        nofilter,
        pidregex,
        ncbi_annot,
        ethresh,
        genomefile,
        namesfile,
        tabsdir,
        datadir,
        divided,
        divmapfile,
        mags):
  print("loading genome list", file=sys.stderr)
  if divided:
    print("loading subdirectory division file", file=sys.stderr)
    with open(divmapfile, 'r') as fh:
      divmap = dict([reversed(line[:-1].split('\t')) for line in fh])
      genomes = divmap.keys()
  else:
    with open(genomefile, "r") as fh:
      genomes = [line[:-1] for line in fh]
  if ncbi_annot:
    print("loading NCBI genome list", file=sys.stderr)
    ncbi = NCBIParse(namesfile)
    print(ncbi["658081"], file=sys.stderr)
    print((list(ncbi.keys())[0:50]), file=sys.stderr)

    print("loading taxonomy and prevalence", file=sys.stderr)
    ncbiT = NCBITaxa()
    taxonomy = oldTaxonomyParse(os.path.join(datadir,
      "taxonomy-with-sp.csv"), ncbiT)
    midas_names = SpeciesInfoParse(os.path.join(datadir, "species_info.txt"))
    phenotype = PhenotypeParse(os.path.join(datadir,
      "suppTable-taxon-scores.csv"))
    prevalence = dict()
    for t in taxonomy.keys():
      mid = taxonomy[t]
      print("mapped: %s -> %s (%s -> %s)" % (t, mid, \
          (ncbi[str(t)] if str(t) in ncbi else "NA"),
          (midas_names[mid] if mid in midas_names else "NA")),
          file=sys.stderr)
      if mid in phenotype:
        prevalences = phenotype[mid]
        try:
          prevalence[t] = max([float(p) for p in prevalences])
        except TypeError:
          prevalence[t] = float(prevalences)
  else:
    ncbi = dict()
  print("\t".join(["Genome",
    "NCBI Taxon ID",
    "NCBI Taxon Name",
    "MIDAS ID",
    "preA protein ID",
    "preT protein ID",
    "preA gene location",
    "preT gene location",
    "preA strand",
    "preT strand",
    "preA score",
    "preT score",
    "max logit-prevalence in gut",
    "neighborhood distance",
    "preA description",
    "preT description"]))
  for g in genomes:
    sys.stderr.write("%s...\n" % g)
    try:
      if mags:
        fmatch = "%s_pre%s_0_output.tab"
        g2 = g
        #fmatch = "%s_pre%s__output.tab"
      else:
        fmatch = "%s_pre%s_0_output.tab"
        g2 = re.sub("_genomic", "", g)
      if divided:
        rd = os.path.join(resultdir, divmap[g])
      else:
        rd = resultdir
      a_hits = HMMParse(os.path.join(rd, fmatch % (g2, "A")),
          pidregex, mags)
      t_hits = HMMParse(os.path.join(rd, fmatch % (g2, "T")),
          pidregex, mags)
    except ValueError:
      print("Skipping %s (no results for either PreA or PreT)" %g, \
          file=sys.stderr)
      continue
    except IOError:
      print("Skipping %s (result file not found)" % g, file=sys.stderr)
      continue
    if divided:
      td = os.path.join(tabsdir, divmap[g])
    else:
      td = tabsdir
    if mags:
      gff_files = [os.path.join(td, "%s.tab" % g)]
    else:
      gff_files = glob.glob(os.path.join(td, "%s*tab" % g))
    if not len(gff_files) == 1:
      print("Skipping %s (missing or multiple GFF file(s))" % g, file=sys.stderr)
      print(gff_files, file=sys.stderr)
      continue
    try:
      gff = GFFParse(gff_files[0], mags)
    except IOError:
      print("Skipping %s (missing or mis-specified GFF file(s))" % g,
          file=sys.stderr)
      continue
    a_hits_passed = set([a for a in a_hits if a_hits[a].e_val < ethresh])
    t_hits_passed = set([t for t in t_hits if t_hits[t].e_val < ethresh])
    if (len(a_hits_passed) == 0) and (len(t_hits_passed) == 0):
      print("No hits passed the threshold for genome %s, skipping" % g,
          file=sys.stderr)
      continue
    for a in a_hits_passed:
      if not a in gff:
        print("...missing protein ID %s in %s, skipping" % \
          (a, g), file=sys.stderr)
        continue
      a_loc = gff[a]["feature_number"]
      for t in t_hits_passed:
        if not t in gff:
          print("...missing protein ID %s in %s, skipping" % (t, g),
              file=sys.stderr)
          continue
        t_loc = gff[t]["feature_number"]
        if nofilter:
          criterion = True # disable checking for preTA
        else:
          if filterexact:
            criterion = ((a_loc <= (t_loc + neighborhood)) and (a_loc >= (t_loc -
              neighborhood)) and (a_loc != t_loc))
          else:
            criterion = ((a_loc <= (t_loc + neighborhood)) and (a_loc >= (t_loc -
              neighborhood)))
        if criterion:
          a_tid = gff[a]["taxon"]
          t_tid = gff[t]["taxon"]
          if a_tid == t_tid:
            if a_tid in ncbi:
              n = ncbi[a_tid]
              try:
                sp = MakeSpecies(a_tid, ncbiT)
                if a_tid in taxonomy:
                  m = taxonomy[a_tid]
                else:
                  m = "NA"
              except ValueError:
                sp = ["NA"]
                m = "NA"
                n = "NA"
            else:
              n = "NA"
              sp = ["NA"]
              m = "NA"
            if ncbi_annot:  
              prevs = [prevalence[s] for s in sp if s in prevalence]
              if len(prevs) > 0:
                prev = "%f" % (max(prevs))
              else:
                prev = "NA"
            else:
              prev = "NA"
            neighborhood_dist = abs(a_loc - t_loc)
            print("%s\t%s\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t%e\t%e\t%s\t%s\t%s\t%s" % \
                (g, a_tid, n, m, a, t, a_loc, t_loc, \
                 gff[a]["strand"], gff[t]["strand"], \
                 a_hits[a].e_val, t_hits[t].e_val, prev, neighborhood_dist,
                 a_hits[a].desc, t_hits[t].desc), flush=True)

if __name__ == "__main__":
  run()
