#!/usr/bin/env Rscript

# make proper inputs to results_to_regions.R

suppressMessages({
    options(tidyverse.quiet = TRUE)
    library(optparse)
    library(tidyverse)
    library(magrittr)
})

if (sys.nframe() == 0) {
    option_list <- list(
        make_option(c("-g", "--divided_genome_file"),
                    type="character", default=NULL,
                    help="divided_[genome_type]_genomes file",
                    metavar="character"),
        make_option(c("-t", "--tab_directory"),
                    type="character", default=NULL,
                    help="path to tab directory *for these genomes*",
                    metavar="character"),
        make_option(c("-f", "--fasta_directory"),
                    type="character", default=NULL,
                    help="path to directory holding .fasta genome files",
                    metavar="character"),
        make_option(c("-x", "--fasta_suffix"), type="character",
                    default=".fna.gz",
                    help="what are the FASTA files called?"),
        make_option(c("-o", "--output_file"), type="character",
                    default="output.txt",
                    help="where to write the output"))
    opt_parser <- OptionParser(option_list=option_list)
    opt <- parse_args(opt_parser)

    opt$tab_directory <- normalizePath(opt$tab_directory)
    opt$fasta_directory <- normalizePath(opt$fasta_directory)
   
    dgf <- read_tsv(opt$divided_genome_file,
                    col_names=c("subdir", "Genome"),
                    col_types="cc")
    dgf %<>% mutate(stub = map_chr(Genome, ~ {
        gsub("(.*)_[^_]+", "\\1", .)
    }))
    dgf %<>% mutate(fasta = map2_chr(stub, Genome, ~ {
        file.path(opt$fasta_directory, .x,
                  paste0(.y, opt$fasta_suffix))
    }))
    dgf %<>% mutate(tab = map2_chr(subdir, Genome, ~ {
        file.path(opt$tab_directory, .x,
                  paste0(.y, ".tab"))
    }))
    write_tsv(dgf, opt$output_file)
}
