#!/bin/bash

DATAPROC=$PROJROOT/data/processed
hmmbuild $DATAPROC/preA-trimmed-automated1.hmm $DATAPROC/preA-trimmed-automated1.clustalw
hmmbuild $DATAPROC/preT-trimmed-automated1.hmm $DATAPROC/preT-trimmed-automated1.clustalw
