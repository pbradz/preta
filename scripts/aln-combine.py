import os, sys
import csv
import re
import glob
import click
import tempfile
import gzip
import string
from functools import reduce
from textwrap import dedent, fill
from Bio import SearchIO, SeqIO, AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import single_letter_alphabet

def KeepOnly(aln, ids, add_if_not=True):
    aln_ids = [a.id for a in aln]
    ncols = aln.get_alignment_length()
    all_gaps = ''.join(["-" for i in range(ncols)])
    not_found = list(set(ids) - set(aln_ids))
    if add_if_not:
        for nf in not_found:
            aln.append(SeqRecord(Seq("-" * ncols, single_letter_alphabet), id=nf))
        aln_ids = [a.id for a in aln]
    else:
        if len(not_found) > 0:
            raise Exception("some IDs were not found: %s" % (format(not_found)))
    finder = dict(zip(aln_ids, range(len(aln))))
    result = AlignIO.MultipleSeqAlignment([aln[finder[id]] for id in ids])
    return(result)

@click.command()
@click.option('--input-format', '-f', help=fill(dedent("""
  Select input alignment format (default: "fasta")
""")), default='fasta')
@click.option('--output-format', '-m', help=fill(dedent("""
  Select output alignment format (default: "fasta")
""")), default='fasta')
@click.option('--output', '-o', help=fill(dedent("""
  Select output file name, or leave as - for stdout
""")), default="-", type=click.File("w"))
@click.option('--keep-all', '-k', help=fill(dedent("""
  Keep all IDs (adding gaps where missing), as opposed to taking intersection
""")), default=False)
@click.argument('inputs', nargs=-1, type=click.File('r'))
def run(input_format, output_format, output, inputs, keep_all):
    alns = [AlignIO.read(f, input_format) for f in inputs]
    ids = [set([a.id for a in aln]) for aln in alns]
    if keep_all:
        common_ids = reduce(lambda x, y: x.union(y), ids)
    else:
        common_ids = reduce(lambda x, y: x.intersection(y), ids)
    if (len(common_ids) < 2):
        raise Exception("Fewer than two ids in common between all files")
    alns_addable = [KeepOnly(aln, common_ids) for aln in alns]
    concatenated = reduce(lambda x, y: x + y, alns_addable)
    output.write(concatenated.format(output_format))

if __name__== "__main__":
    run()
