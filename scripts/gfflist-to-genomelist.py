#!/usr/bin/env python

import os, sys, re

for line in sys.stdin:
    (subdir, gff) = line[:-1].split('\t')
    print("{}\t{}".format(subdir,
                          re.sub(".gff", "",
                                 re.sub(".gz", "", os.path.basename(gff)))))
