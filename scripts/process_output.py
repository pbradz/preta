#!/usr/bin/python

import sys, os
import csv
import re
import glob
from Bio import SearchIO
from ete3 import NCBITaxa

EThresh = 1e-2
GenomeFile = "/pollard/home/pbradz/projects/preta/data/hmm-output/genomes.txt"
NamesFile = "/pollard/home/pbradz/data/ncbi/names.dmp"
TabsDir = "/pollard/home/pbradz/projects/preta/data/parsed-gffs/tabs"
HMMOutDir = "/pollard/home/pbradz/projects/preta/data/hmm-output/"
ResultDir = sys.argv[1]

def CommentStripper(iterator):
  for line in iterator:
    if line.startswith('#'):
      continue
    if not line.strip():
      continue
    yield line


def HMMParse(filename):
  hits = dict()
  results = SearchIO.read(filename, "hmmer3-tab")
  for result in results:
    desc = result.description
    e_val = float(result.evalue)
    parsed = re.findall("(\[[^\]]*=[^\]]*\])", desc)
    for p in parsed:
      m = re.match("\[(.*)=(.*)\]", p)
      if not m == None:
        if (m.group(1) == "protein_id"):
          hits[m.group(2)] = e_val
  return(hits)

def NCBIParse(filename):
  taxa = dict()
  with open(filename, 'rb') as fh:
    lastorg = "NA" 
    for line in fh:
      rem_end_delim = re.sub("\t\|\n", "", line)
      row = rem_end_delim.split("\t|\t")
      org = str(row[0])
      if row[3] == "scientific name":
        taxa[org] = row[1]
      if (not (org == lastorg)) and \
          (not(lastorg == "NA")) and \
          (not taxa.has_key(lastorg)):
        taxa[lastorg] = "node #%s" % org
      lastorg = org
  return(taxa)

def GFFParse(filename):
  proteins = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      if not row["protein_id"] == "None":
        pid = row["protein_id"]
        proteins[pid] = dict()
        proteins[pid]["taxon"] = row["taxon"]
        proteins[pid]["gene_number"] = int(row["gene_number"])
        proteins[pid]["strand"] = row["strand"]
  return(proteins)

def oldTaxonomyParse(filename, taxonomy):
  species_to_midas = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      gid = str(row["taxon_id"])
      species = MakeSpecies(gid, taxonomy)
#      lineage = taxonomy.get_lineage(gid)
#      ranks = taxonomy.get_rank(gid)
#      species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
#          if i == "species"]
#      print >> sys.stderr, "%s == %s == %s" % (gid, species, row["species_id"])
      if (len(species) == 1):
        species_to_midas[str(species[0])] = str(row["species_id"])
  return(species_to_midas)

def TaxonomyParse(filename):
  species_to_midas = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      species_to_midas[row["species_name"]] = str(row["species_id"])
  return(species_to_midas)

def SpeciesInfoParse(filename):
  midas_names = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      midas_names[row["species_id"]] = row["species_name"]
  return(midas_names)

def PhenotypeParse(filename):
  phenotype = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      phenotype[row["ID"]] = float(row["logit.Prevalence"])
  return(phenotype)

def MakeSpecies(taxon, taxonomy):
  lineage = taxonomy.get_lineage(taxon)
  ranks = taxonomy.get_rank(lineage)
  species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
      if i == "species"]
  return(species)


# read genomes file
print >> sys.stderr, "loading genome list"
with open(GenomeFile, "rb") as fh:
  genomes = [line[:-1] for line in fh]

print >> sys.stderr, "loading NCBI genome list"
ncbi = NCBIParse(NamesFile)
print >> sys.stderr, ncbi["658081"]
print >> sys.stderr, (ncbi.keys()[0:50])

print >> sys.stderr, "loading taxonomy and prevalence"
ncbiT = NCBITaxa()
taxonomy = oldTaxonomyParse(os.path.join(HMMOutDir,
  "taxonomy-with-sp.csv"), ncbiT)
invtax = dict([reversed(i) for i in taxonomy.items()])
# print(dict(invtax.items()[0:10]))
midas_names = SpeciesInfoParse(os.path.join(HMMOutDir, "species_info.txt"))
phenotype = PhenotypeParse(os.path.join(HMMOutDir,
  "suppTable-taxon-scores.csv"))
prevalence = dict()
for t in taxonomy.keys():
  mid = taxonomy[t]
  print >> sys.stderr, "mapped: %s -> %s (%s -> %s)" % (t, mid, \
      (ncbi[str(t)] if ncbi.has_key(str(t)) else "NA"),
      (midas_names[mid] if midas_names.has_key(mid) else "NA"))
  if phenotype.has_key(mid):
    prevalences = phenotype[mid]
    try:
      prevalence[t] = max([float(p) for p in prevalences])
    except TypeError:
      prevalence[t] = float(prevalences)


print "\t".join(["Genome",
  "NCBI Taxon ID",
  "NCBI Taxon Name",
  "MIDAS ID",
  "preA protein ID",
  "preT protein ID",
  "preA gene location",
  "preT gene location",
  "preA strand",
  "preT strand",
  "preA score",
  "preT score",
  "max logit-prevalence in gut"])
for g in genomes:
  try:
    a_hits = HMMParse(os.path.join(ResultDir, "%s_preA_0_output.tab" % g))
    t_hits = HMMParse(os.path.join(ResultDir, "%s_preT_0_output.tab" % g))
  except ValueError:
    print >> sys.stderr, "Skipping %s (no results for either PreA or PreT)" %g
    continue
  except IOError:
    print >> sys.stderr, "Skipping %s (result file not found)" % g
    continue
  gff_files = glob.glob(os.path.join(TabsDir, "%s*tab" % g))
  if not len(gff_files) == 1:
    print >> sys.stderr, "Skipping %s (missing or multiple GFF file(s))" % g
    continue
  try:
    gff = GFFParse(gff_files[0])
  except IOError:
    print >> sys.stderr, "Skipping %s (missing or mis-specified GFF file(s))" % g
    continue
  a_hits_passed = set([a for a in a_hits.keys() if a_hits[a] < EThresh])
  t_hits_passed = set([t for t in t_hits.keys() if t_hits[t] < EThresh])
  try:
    g_taxon = gff[gff.keys()[0]]
  except:
    g_taxon = "NA"
  for a in a_hits_passed:
    if not gff.has_key(a):
      print >> sys.stderr, "...missing protein ID %s in %s (%s), skipping" % \
        (a, g, g_taxon)
      continue
    a_loc = gff[a]["gene_number"]
    for t in t_hits_passed:
      if not gff.has_key(t):
        print >> sys.stderr, "...missing protein ID %s in %s, skipping" % (t, g)
        continue
      t_loc = gff[t]["gene_number"]
      if (a_loc == (t_loc + 1)) or (a_loc == (t_loc - 1)):
      #if (a_loc == t_loc):
        a_tid = gff[a]["taxon"]
        t_tid = gff[t]["taxon"]
        if a_tid == t_tid:
          if ncbi.has_key(a_tid):
            n = ncbi[a_tid]
            sp = MakeSpecies(a_tid, ncbiT)
#            print >> sys.stderr, "%s -- %s -- %s" % (n, a_tid, sp)
            if taxonomy.has_key(a_tid):
              m = taxonomy[a_tid]
            else:
              m = "NA"
          else:
            n = "NA"
            sp = ["NA"]
            m = "NA"
          prevs = [prevalence[s] for s in sp if prevalence.has_key(s)]
          if len(prevs) > 0:
            prev = "%f" % (max(prevs))
          else:
            prev = "NA"
          print "%s\t%s\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t%e\t%e\t%s" % \
              (g, a_tid, n, m, a, t, a_loc, t_loc, \
               gff[a]["strand"], gff[t]["strand"], \
               a_hits[a], t_hits[t], prev)

