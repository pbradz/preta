#!/usr/bin/env python3

# Results to regions script
# -------------------------
# If run in regular mode, for each entry in a "summarized results" table:
#  * Find and open the relevant genome .tab file (generated from GFFs)
#  * Find the smallest genomic region that completely covers preTA up to
#    the nearest other genetic element
#  * Append the nucleotide sequence of that region to one file
#
# If run in reverse mode, perform the same action for preTs and preAs in an
# input list that are NOT in the "summarized results" table.
#

# Function: get a specific sequence

import click
import os
import sys
import pandas as pd
from Bio import SeqIO


def GetRecords(handle, fmt="fasta"):
    return([record for record in SeqIO.parse(handle, fmt)])

def SplitRegion(genomefile, start, finish, sense=True,
                which=None, records=None):
    assert start < finish, \
      "start should be before finish (use sense to flip strand)"
    if not records: # not provided, so read them in
        with open(genomefile, "rU") as fh:
            records = GetRecords(fh)
    if which:
        match = [r for r in records if r.name == which]
        assert match, "record not found"
        # assume it's the first match if multiple (shouldn't be)
        record = match[0]
    else: # assume it's the first/only one
        record = records[0]
    region = record.seq[start:finish]
    if sense:
        return(region.reverse_complement())
    return(region)

# Known issue is that this won't get intergenic sequence before the first gene or after the last gene on a contig; this is unavoidable without re-parsing the GFF files
def GenePlusNeighborhood(gene, tabs):
    gene_entry = tabs[tabs.gene_id == gene]
    assert len(gene_entry) == 1, "gene ID not found or not unique"
    sequence = gene_entry.iloc[0]["sequence_id"]
    fnum = gene_entry.iloc[0]["feature_number"]
    same_seq = tabs[tabs.sequence_id == sequence]
    if (fnum - 1) in same_seq["feature_number"]:
        prev_g = same_seq[same_seq.feature_number == (fnum - 1)]
        prev_end = prev_g.iloc[0]["stop"] + 1
    else:
        prev_end = gene_entry.iloc[0]["start"]
    if (fnum + 1) in same_seq["feature_number"]:
        next_g = same_seq[same_seq.feature_number == (fnum + 1)]
        next_end = next_g.iloc[0]["start"] - 1
    else:
        next_end = gene_entry.iloc[0]["stop"]
    return(prev_end, next_end)
    #df = pd.DataFrame({'prev':[prev_end], 'next':[next_end]})
    #return(df)

def Combine():
    pass

# featurepairs is a list of gene_ids
def GetRegions(tabfile, featurelist):
    tabs = pd.read_table(tabfile)
    relevant = tabs[tabs.gene_id.isin(featurelist)]
    relevant['prev'], relevant['next'] = zip(
        *relevant['gene_id'].map(
            lambda x: GenePlusNeighborhood(x, tabs)))
    rel_g = relevant.groupby("sequence_id")
    maxstop = rel_g["stop"].max(axis=0)
    minstart = rel_g["start"].min(axis=0)
    modestrand = rel_g["strand"].apply(lambda x: x.mode())
    print(modestrand)
    print([minstart, maxstop, modestrand])
    results = pd.concat([minstart, maxstop, modestrand], axis=1)
    return(results.reset_index())




# how did i separate out before? --> used fasta of all hits, maybe can do this separately...


@click.command()
@click.option('--results', help='Results table from running HMMs')
@click.option('--genome-table',
              help='Table relating genomes to location of full genome') # necessary?
@click.option('--tab-table',
              help='Table relating genomes to tabs (parsed GFFs)')
def run(results, genome_table, tab_table):
    rtable = pd.read_table(results)
    gtable = pd.read_table(genome_table)
    ttable = pd.read_table(tab_table)
    rgttable = rtable.merge(
        gtable, on='Genome', how='inner').merge(
            ttable, on='Genome, how=inner')
    # now we have a master table relating the preTA results to file names
    # note: still need to generate these tables
    # note: next step is mutating to get the seq records in a column


    print(whatever)

if __name__ == '__main__':
    run()
