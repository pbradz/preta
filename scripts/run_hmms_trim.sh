#!/bin/bash

GFFLIST="$1"
NSEQFILE="$2"
PREA="$3"
PRET="$4"
WILDCARD="$5"
OUTDIR="$6"
NSEQS=`cat $NSEQFILE`
mkdir -p $OUTDIR
mkdir -p $OUTDIR/logs
for x in `cat $GFFLIST`; do
    y=`dirname $x`;
    n=`basename $x`;
#    z=("${y}/*translated_cds*gz");
    z=("${y}/$WILDCARD");
    echo $n
    count=0
    for Z in $z; do
        echo ... $Z
        hmmsearch -Z $NSEQS --tblout $OUTDIR/${n}_preA_${count}_output.tab $PREA $Z > $OUTDIR/logs/${n}_preA_trimmed_auto1.log
        hmmsearch -Z $NSEQS --tblout $OUTDIR/${n}_preT_${count}_output.tab $PRET $Z > $OUTDIR/logs/${n}_preT_trimmed_auto1.log
        (( count++ ))
    done
done

