# Get eukaryotic representative genomes of good quality

library(tidyverse)

assemblies <- read_tsv("../data/assembly_summary_refseq.txt",
                       skip = 1,
                       comment = '',
                       quote = '')

complete <- filter(assemblies,
                   assembly_level %in% c("Chromosome",
                                         "Complete Genome") &
                   genome_rep == "Full")

Sys.setenv(ENTREZ_KEY='b2ea86a74c9b5eca8ac3420fe91faebd4609')
annotations <- classification(complete$taxid, db="ncbi")
saveRDS(file="annotations.rds", annotations)

superkingdoms <- vapply(annotations, function(x) {
    if (is.null(dim(x))) { return("NA") }
    filter(x, rank=="superkingdom")[["name"]]
}, FUN.VALUE="")

euks <- names(which(superkingdoms == "Eukaryota"))
archaea <- names(which(superkingdoms == "Archaea"))
GCFs <- filter(assemblies,
               taxid %in% c(euks, archaea))[[1]]
directories <- filter(assemblies,
                      taxid %in% c(euks, archaea))[["ftp_path"]]

write(directories, file="directory_list.csv")
