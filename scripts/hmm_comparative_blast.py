#!/usr/bin/env python

## Keep reads from a fastq file matching input (multi-)HMMs

import os, sys, subprocess, copy
import csv
import re
import glob
import click
import tempfile
import gzip
from Bio import SearchIO, SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna, generic_protein
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from Bio.Seq import reverse_complement, transcribe, back_transcribe, translate

## Function and class definitions

def HitsPassingCutoff(filename, cutoff):
    rparse = SearchIO.parse(filename, "hmmer3-tab")
    resultlist = [FilterHits(r, cutoff) for r in rparse]
    return(resultlist)

def FilterHits(result, cutoff):
  return([h for h in result.hits if h.evalue <= cutoff])

def GetMarkers(hmm):
    names = set()
    with open(hmm, 'r') as fh:
        for f in fh:
            m = re.match('NAME +(.*)$', f)
            if not m is None:
                names.add(m.group(1))
    return(list(names))

def MaybeGzipOpen(fn):
    if fn.endswith(".gz"):
        return(gzip.open(fn, 'rt'))
    else:
        return(open(fn))

def SixFrameTranslate(fastq, outfile, encoding, tbl='Standard'):
    with MaybeGzipOpen(fastq) as ifh:
        with open(outfile, "w") as ofh:
            for title, seq, qual in FastqGeneralIterator(ifh):
                tsplit = title.split(" ")
                seqs = [seq] * 6
                for i in range(0, 3):
                    seqs[i] = seqs[i][i:]
                for i in range(3, 6):
                    seqs[i] = reverse_complement(seqs[i])[(i-3):]
                for i in range(0, 6):
                    l = len(seqs[i])
                    if (l % 3) > 0:
                        seqs[i] = seqs[i] + ('N' * (3 - (l % 3)))
                    #tl = l - (l % 3)
                    #seqs[i] = seqs[i][:tl]
                    seqs[i] = translate(seqs[i], tbl, stop_symbol='X')
                for (n, s) in enumerate(seqs):
                    nsplit = tsplit.copy()
                    nsplit[0] = nsplit[0] + ("_%d" % (n + 1))
                    ofh.write(">%s\n%s\n" % (" ".join(nsplit), s))
    ifh.close()
    return

## Program body

@click.command()
@click.option('--fastq-file', '-f', 'fastq_file',
              help='Original FASTQ file')
@click.option('--transeq-file', '-F', 'transeq_file', default=None,
              help='Sixframe-translated FASTQ file (optional)')
@click.option('--trans-table', '-T', 'trans_table', default='Standard',
              help='Codon table to use for translation (name or NCBI ID)')
@click.option('--marker-file', '-p', 'marker_file', default='phyeco.hmm',
              help='Multi-HMM file containing markers/genes to find')
@click.option('--output-file', '-o', 'output_file', default='filtered.fq',
              help='Where to store filtered FASTQ')
@click.option('--tmp-dir', '-t', 'tmp_dir', default='/tmp/',
              help='Directory to store temporary HMMER3 files')
@click.option('--verbose', '-v', 'verbose', default=0,
              help='Set verbosity, where 0 = none, 1 = monitor, 2 = debug')
@click.option('--cutoff', '-c', 'cutoff', default=1.00,
              help='Set e-value cutoff for results')
@click.option('--encoding', '-e', 'encoding', default='fastq-sanger',
              help='Set type of FASTQ file (see BioPython documentation)')
def run(fastq_file, marker_file, output_file, transeq_file,
        trans_table, tmp_dir, verbose, cutoff, encoding):
    markers = GetMarkers(marker_file)
    ml = len(markers)
    transeq_file_h = None
    if not transeq_file:
      if verbose > 0: click.echo("Sixframe translating...")
      transeq_file_h=tempfile.mkstemp(dir=tmp_dir)
      transeq_file=transeq_file_h[1]
      ## Used to use transeq here but memory usage is insane for large files
      #  os.system("transeq -sequence %s -outseq %s -frame 6 -clean" %
      #      (fastq_file, transeq_file))
      SixFrameTranslate(fastq_file, transeq_file, encoding, tbl=trans_table)
    out_file=tempfile.mkstemp(dir=tmp_dir)
    if verbose > 0: click.echo("Performing HMM search against sixframe...")
    os.system("hmmsearch --cpu 1 --tblout=%s %s %s > /dev/null" %
                (out_file[1], marker_file, transeq_file))
    rlist = HitsPassingCutoff(out_file[1], cutoff)
    os.close(out_file[0])
    if transeq_file_h:
      os.close(transeq_file_h[0])
    if verbose > 1: click.echo("  ...%d/%d markers found" % (len(rlist), ml))
    for (n, r) in enumerate(rlist):
        if verbose > 1:
            click.echo("  ==> marker %d: %d copies passed" % (n, len(r)))
    if verbose > 1: click.echo(str(rlist))
    matches = set()
    for hits in rlist:
        for hit in hits:
            matches.add(re.sub("(.*)_.*", "\\1", hit.id))
    if verbose > 1: click.echo(str(matches))
    if len(matches) == 0:
        click.echo("No matches found, creating empty result file")
        open(output_file, "a").close()
        return
    if verbose > 0: click.echo("Filtering original sequences...")
    with MaybeGzipOpen(fastq_file) as ifh:
        with open(output_file, "w") as ofh:
            for record in SeqIO.parse(ifh, encoding):
                if verbose > 2: click.echo(record.id)
                if record.id in matches:
                    if verbose > 1: click.echo("found match %s" % record.id)
                    SeqIO.write(record, ofh, encoding)

if __name__ == '__main__':
    run()
