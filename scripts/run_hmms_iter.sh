#!/bin/bash

PRETA_ROOT=$HOME/projects/preta/

for x in `cat $PRETA_ROOT/data/hmm-output/genomes.txt`; do
	y=("/pollard/home/jshi/db/refseq/complete_lastest_bact/all_records/$x*");
  z=("${y[0]}/*translated_cds*gz")
  echo $x
	count=0
	for Z in $z; do
		echo ... $Z
		hmmsearch -Z 33383393 --tblout $PRETA_ROOT/data/hmm_output/nhf_iter_output/${x}_preA_${count}_output.tab $PRETA_ROOT/sequences/iterative/preA-nohuman-filter-5-alignment.hmm $Z > $PRETA_ROOT/data/hmm-output/logs/${x}_preA_nhf_iter.log
		hmmsearch -Z 33383393 --tblout $PRETA_ROOT/data/hmm_output/nhf_iter_output/${x}_preT_${count}_output.tab $PRETA_ROOT/sequences/iterative/preT-nohuman-filter-5-alignment.hmm $Z > $PRETA_ROOT/data/hmm-output/logs/${x}_preT_nhf_iter.log
		(( count++ ))
	done
done
