#!/bin/sh

find $1 -name "*feature_count*txt.gz" -exec zgrep "^CDS with_protein" {} \; > $2
