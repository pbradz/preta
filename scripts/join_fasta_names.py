#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
def process(infile, outfile):
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      i = record.id
      d = " ".join(record.description.split(" ")[1:])
      r = copy.deepcopy(record)
      unify = "{}!!{}".format(n, d)
      r.name = unify
      r.id = unify
      r.description = ""
      SeqIO.write(r, oh, "fasta")
  oh.close()    

if __name__ == '__main__':
  process()
