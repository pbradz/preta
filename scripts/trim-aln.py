#!/usr/bin/env python
import os, sys
import csv
import re
import glob
import click
import tempfile
import gzip
import string
from textwrap import dedent, fill
from Bio import SearchIO, SeqIO, AlignIO

def GapThresh(s, t, v):
    if (v > 1): sys.stderr.write("%s: " % format(s))
    gfrac = float(s.count('-')) / len(s)
    if (v > 1): sys.stderr.write("fraction of gaps: %s \n" %
                                 format(gfrac))
    if (v > 1): sys.stderr.write("keep: %s \n" %
                                 format(gfrac <= t))
    return(gfrac <= t)

def ConsThresh(s, t, v):
    if (v > 1): sys.stderr.write("%s: " % format(s))
    uniqchars = set(s) - set("-")
    cfrac = [float(s.count(c)) / len(s) for c in uniqchars]
    if (v > 1): sys.stderr.write("highest consensus: %s \n" %
                                 format(max(cfrac)))
    if (v > 1): sys.stderr.write("keep: %s \n" %
                                 format(max(cfrac) >= t))
    return(max(cfrac) >= t)

def SubSeqRec(seqrec, indices):
    s = str(seqrec.seq)
    r = ''.join([s.__getitem__(i) for i in indices])
    s2 = seqrec.__radd__(r)[0:len(r)]
    return(s2)

def SubAln(aln, indices):
    seqrecs = [SubSeqRec(aln[r], indices) for r in range(len(aln))]
    return(AlignIO.MultipleSeqAlignment(seqrecs))

@click.command()
@click.option('--in-file', '-i', help='Path to input file')
@click.option('--out-format', '-f', help=fill(dedent("""
  Set output format ("clustal", "emboss", "fasta", "stockholm", or any format
  described at https://biopython.org/wiki/AlignIO)
""")), default="clustal")
@click.option('--in-format', '-m', help=fill(dedent("""
  Set input format ("clustal", "emboss", "fasta", "stockholm", or any format
  described at https://biopython.org/wiki/AlignIO)
""")), default="clustal")
@click.option('--consensus-threshold', '-c', help=fill(dedent("""
  Consensus threshold, as a decimal from 0 to 1 (remove columns without at
  least this fraction consensus)
""")), default=0.0)
@click.option('--gap-threshold', '-g', help=fill(dedent("""
  Gap removal threshold, as a decimal from 0 to 1 (remove columns with at least
  this fraction gaps)
""")), default=1.0)
@click.option('--seq-threshold', '-s', help=fill(dedent("""
  Threshold for deleting sequences entirely, as a decimal from 0 to 1 (remove
  sequences with at least this fraction gaps)
""")), default=1.0)
@click.option('--verbose', '-v', help=fill(dedent("""
  Set verbosity from 0 to 2
""")), default=0)
def run(in_file,
        in_format,
        out_format,
        consensus_threshold,
        gap_threshold,
        seq_threshold,
        verbose):
    with open(in_file, 'r') as fh:
        aln = AlignIO.read(fh, in_format)
        ncol = aln.get_alignment_length()
        if (verbose>1): sys.stderr.write("%d cols\n" % (ncol))
        nrow = len(aln)
        if (verbose>1): sys.stderr.write("%d rows\n" % (nrow))
        if (verbose>0): sys.stderr.write("Trimming columns...\n")
        valid_cols = [col for col in range(ncol) if
                      (GapThresh(aln[:, col], gap_threshold, verbose) and \
                       ConsThresh(aln[:, col], consensus_threshold, verbose))]
        if (len(valid_cols) < 2):
            raise Exception('Fewer than two columns survived, check parameters')
        if (verbose>1): sys.stderr.write(format(valid_cols))
        alnC = SubAln(aln, valid_cols)
        if (verbose>0): sys.stderr.write("Trimming rows...\n")
        valid_rows = [row for row in range(nrow) if
                      GapThresh(str(alnC[row, :].seq), seq_threshold, verbose)]
        if (verbose>1): sys.stderr.write(format(valid_rows))
        if (len(valid_rows) < 2):
            raise Exception('Fewer than two rows survived, check parameters')
        alnCR = AlignIO.MultipleSeqAlignment([alnC[r] for r in valid_rows])
        print(alnCR.format(out_format))

if __name__== "__main__":
    run()
