#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
@click.option('--matchon', '-m', type=click.Choice(['name', 'desc', 'd2',
  'seq', 'nameseq']), default='nameseq')
@click.option('--summarize/--nosummarize', '-s', default=False)
@click.option('--upper/--noupper', default=False)
@click.option('--length', default=float('-inf'))
def process(infile, outfile, matchon, summarize, upper, length):
  ndups = 0
  nwritten = 0
  records = dict()
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      d = " ".join(record.description.split(" ")[1:])
      s = str(record.seq)
      if len(set(s) - set(['-', '*', 'X'])) == 0:
          sys.stderr.write("Warning: skipping empty entry %s\n" % (m))
          continue
      if (matchon == 'name') or (matchon == 'nameseq'):
        m = n
        if matchon == 'name':
          r = n
        else:
          r = s
      elif matchon == 'seq':
        m = s
        r = s
      elif matchon == 'desc':
        m = d
        r = d
      elif matchon == 'd2':
        m = d.split(' ')[1]
        r = d.split(' ')[1]
      if upper:
        record.seq = record.seq.upper()
      if m not in records:
        if s == '':
          sys.stderr.write("Warning: dropping empty entry %s\n" % (m))
        elif len(s) < length:
          sys.stderr.write("Warning: dropping short entry %s\n" % (m))
        else:
          SeqIO.write(record, oh, "fasta")
          nwritten += 1
          records[m] = r
      else:
        if records[m] != r:
          sys.stderr.write("Warning: renaming record %s!!%s\n" % (n, d))
          record.name = "%s!!%s" % (n, d)
          if s == '':
            sys.stderr.write("Warning: dropping empty entry %s\n" % (m))
          elif len(s) < length:
            sys.stderr.write("Warning: dropping short entry %s\n" % (m))
          else:
            SeqIO.write(record, oh, "fasta")
            nwritten += 1
            records[record.name] = r
        else:
          ndups += 1
          if not summarize:
            sys.stderr.write("Duplicate of %s found, skipping\n" % r)
  if summarize:
    sys.stderr.write("%d duplicates removed; %d entries written\n" % (ndups,
      nwritten))
  oh.close()    

if __name__ == '__main__':
  process()
