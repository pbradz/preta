#!/usr/bin/env Rscript

# Do post-hoc filtering on a MAG list so we don't have to re-generate tabs, etc.

suppressMessages({
    options(tidyverse.quiet = TRUE)
    library(optparse)
    library(tidyverse)
    library(magrittr)
})

if (sys.nframe() == 0) {
    option_list <- list(
        make_option(c("-d", "--divided"),
                    type="logical", default=FALSE,
                    help="is this a divided file?"),
        make_option(c("-i", "--input_file"),
                    type="character", default=NULL,
                    help="file to filter",
                    metavar="character"),
        make_option(c("-t", "--restrict_table"),
                    type="character", default=NULL,
                    help="MAG table to use",
                    metavar="character"),
        make_option(c("-B", "--backup_file"), type="character",
                    default="backup.txt",
                    help="where to write the backup file"))
    opt_parser <- OptionParser(option_list=option_list)
    opt <- parse_args(opt_parser)

    opt$input_file <- normalizePath(opt$input_file)
    opt$backup_file <- normalizePath(opt$backup_file)
    opt$restrict <- normalizePath(opt$restrict)

    suppressMessages({
        input <- read_tsv(opt$input_file, col_names=FALSE)
        restrict <- read_tsv(opt$restrict, na="NULL")
    })

    if (!(file.copy(opt$input_file, opt$backup_file, overwrite=FALSE))) {
        stop(paste0("Error backing up input file ",
                    opt$input_file,
                    " to ",
                    opt$backup_file,
                    "; aborting"))
    }

    which_col <- ifelse(opt$divided, "X2", "X1")
    b <- mutate(input, basenames = map_chr(input[[which_col]], ~ {
        strsplit(basename(.), "\\.")[[1]][1]
    }))
    f <- filter(restrict, quality_level %in% c("high", "medium"))
    a <- filter(b, basenames %in% f$genome_name) %>%
        select(-basenames)
    message(paste0("Reduced ", nrow(input), " lines to ", nrow(a)))
    write_tsv(a, opt$input_file, col_names=FALSE)
}
