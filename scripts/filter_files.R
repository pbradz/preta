#!/usr/bin/env Rscript

# filter GFFs to a known set

suppressMessages({
    options(tidyverse.quiet = TRUE)
    library(optparse)
    library(tidyverse)
    library(magrittr)
})

if (sys.nframe() == 0) {
    option_list <- list(
        make_option(c("-g", "--gff_list"),
                    type="character", default=NULL,
                    help="gff list file",
                    metavar="character"),
        make_option(c("-r", "--restrict"),
                    type="character", default=NULL,
                    help="list of file basenames to keep",
                    metavar="character"),
        make_option(c("-o", "--output_file"), type="character",
                    default="output.txt",
                    help="where to write the output"))
    opt_parser <- OptionParser(option_list=option_list)
    opt <- parse_args(opt_parser)

    opt$gff_list <- normalizePath(opt$gff_list)
    opt$restrict <- normalizePath(opt$restrict)

    gffs <- read_tsv(opt$gff_list, col_names=FALSE)
    restrict <- read_tsv(opt$restrict, col_names=FALSE)
    gffs <- mutate(gffs, X2 = basename(X1))
    j <- inner_join(gffs, restrict, by=c("X2"="X1"))
    write_tsv(select(j, "X1"), opt$output, col_names=FALSE)
}
