#!/bin/bash

PRETA_ROOT=$HOME/projects/preta/

for x in `cat $PRETA_ROOT/scripts/euk_arc_genomes.txt`; do
	y=("/pollard/data/eukaryote_and_archaeal_genomes/refseq/$x*");
  z=("${y[0]}/*protein*gz")
  echo $x
	count=0
	for Z in $z; do
		echo ... $Z
		hmmsearch -Z 7938101 --tblout $PRETA_ROOT/data/hmm-output/trim_euk_output/${x}_preA_${count}_output.tab $PRETA_ROOT/sequences/preA-trimmed-automated1.hmm $Z > $PRETA_ROOT/data/hmm-output/logs/${x}_preA_trimmed_euk_auto1.log
		hmmsearch -Z 7938101 --tblout $PRETA_ROOT/data/hmm-output/trim_euk_output/${x}_preT_${count}_output.tab $PRETA_ROOT/sequences/preT-trimmed-automated1.hmm $Z > $PRETA_ROOT/data/hmm-output/logs/${x}_preT_trimmed_euk_auto1.log
		(( count++ ))
	done
done
