#!/usr/bin/env python

import os, sys, subprocess, copy
import csv
import re
import glob
import click
import tempfile
import gzip
import pathlib
from Bio import SearchIO, SeqIO
# from ete2 import NCBITaxa

## Function and class definitions

def BestHit(result):
    return([max(result.hits, key=lambda h: h.bitscore)])

def BestHitsFromResult(filename):
    rparse = SearchIO.parse(filename, "hmmer3-tab")
    resultlist = [BestHit(r) for r in rparse]
    return(resultlist)

def HitsPassingCutoff(filename, cutoff):
    rparse = SearchIO.parse(filename, "hmmer3-tab")
    resultlist = [FilterHits(r, cutoff) for r in rparse]
    return(resultlist)

def FilterHits(result, cutoff):
  return([h for h in result.hits if h.evalue <= cutoff])

class Genome:
    def __init__(self, name, path):
        self.name = name
        self.path = path

def GetMarkers(hmm):
    names = set()
    with open(hmm, 'r') as fh:
        for f in fh:
            m = re.match('NAME +(.*)$', f)
            if not m is None:
                names.add(m.group(1))
    return(list(names))

def LineToGenome(line):
    gf = line.rstrip()
    n = os.path.splitext(os.path.basename(gf))[0]
    return(Genome(n, gf))

def GFProcess(gfile):
    with open(gfile, 'r') as fh:
        glist = [LineToGenome(line) for line in fh]
    return(glist)

def GetRecords(g, hitlist, verbose=0):
  queries = set()
  for hits in hitlist:
    for hit in hits:
      queries.add(hit.query_id)
  queries = list(queries)    
  query_dict = dict(zip(queries, [[] for q in queries]))
  e = os.path.splitext(os.path.basename(g.path))[1]
  if e == ".gz":
    if verbose > 1: click.echo("Using gzip to read %s" % (g.path))
    fh = gzip.open(g.path, 'rt')
  else:
    if verbose > 1: click.echo("Using a regular filestream to open %s" % (g.path))
    fh = open(g.path, 'r')
  for record in SeqIO.parse(fh, "fasta"):
    for hits in hitlist:
      for hit in hits:
        if (record.id == hit.id):
          if verbose > 1: click.echo("%s found (%s)" % (hit.query_id, hit.id))
          thisrec = copy.deepcopy(record)
          thisrec.description = "!!".join([g.name, thisrec.description,
            'evalue: {}'.format(hit.evalue)])
          thisrec.id = g.name
          if verbose > 1: click.echo(str(thisrec))
          query_dict[hit.query_id].append(thisrec)
  fh.close()
  return(query_dict)

## Program body

@click.command()
@click.option('--genome-file', '-g', 'genome_file', default='genomes.txt',
              help='Return-delimited list of paths to genome files')
@click.option('--marker-file', '-p', 'marker_file', default='phyeco.hmm',
              help='Multi-HMM file containing phyeco markers')
@click.option('--output-dir', '-o', 'output_dir', default='./scan_results/',
              help='Directory to place output FASTA files')
@click.option('--verbose', '-v', 'verbose', default=0,
              help='Set verbosity, where 0 = none, 1 = monitor, 2 = debug')
@click.option('--cutoff', '-c', 'cutoff', default=1.00,
              help='Set e-value cutoff for results')
@click.option('--only-best/--return-all', '-b', 'only_best', default=True,
              help='Return only best hit (as opposed to returning all ' 
              'passing the threshold)')
def run(genome_file, marker_file, output_dir, verbose, cutoff, only_best):
    markers = GetMarkers(marker_file)
    ml = len(markers)
    genomes = GFProcess(genome_file)
    pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)  
    output_handles = [open(os.path.join(output_dir, m), 'w') for m in markers]
    outputs = dict(zip(markers, output_handles))
    (out_fh, out_file) = tempfile.mkstemp(text=True)
    os.close(out_fh)
    logfh = open(os.path.join(output_dir, "phyeco_scanner.log"), 'w')
    lg = len(genomes)
    for (gn, g) in enumerate(genomes):
        if verbose > 0: click.echo("Processing genome %d/%d: %s (%s)" %
                                   (gn + 1, lg, g.name, g.path))
        e = os.path.splitext(g.path)[1]
        if (e == ".gz"):
            usingTemp = True
            (in_fh, in_path) = tempfile.mkstemp(text=True)
            os.close(in_fh)
            os.system("zcat %s > %s" % (g.path, in_path))
        else:
            usingTemp = False
            in_path = g.path
        os.system("hmmsearch --tblout=%s %s %s > /dev/null" %
                  (out_file, marker_file, in_path))
        if usingTemp:
            os.system("rm %s" % (in_path))
        if only_best:
          rlist = BestHitsFromResult(out_file)
        else:
          rlist = HitsPassingCutoff(out_file, cutoff)
        logfh.write("%s\t%s\n" % (g.name, len(rlist)))
        if verbose > 1: click.echo("  ...%d/%d markers found" % (len(rlist), ml))
        if verbose > 1: click.echo(str(rlist))
        for (n, r) in enumerate(rlist):
          if verbose > 1:
            click.echo("  ==> marker %d: %d copies passed" % (n, len(r)))
          logfh.write("  => marker %d: %d copies passed\n" % (n, len(r))) 
        logfh.write("%s\t%s\n" % (g.name, len(rlist)))
        rdict = GetRecords(g, rlist, verbose)
        if verbose > 1: click.echo(str(rdict))
        for marker in rdict.keys():
          handle = outputs[marker]
          records = rdict[marker]
          if len(records) > 0:
            for record in records:
              SeqIO.write(record, handle, "fasta")
    for handle in output_handles: handle.close()

if __name__ == '__main__':
    run()
