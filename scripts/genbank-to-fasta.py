#!/usr/bin/python

import os
import sys
import argparse

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import gzip
import operator

def feature_gen(features, accessions):
  for (fn, feature) in enumerate(features):
      if feature.type == 'CDS':
        try:
          if feature.qualifiers.has_key("note"):
            desc = "%s; %s" % (feature.qualifiers["product"][0], \
                feature.qualifiers["note"][0])
          else:
            desc = feature.qualifiers["product"][0]
        except:
          desc = "unannotated"
        if feature.qualifiers.has_key("translation"):
          translation = feature.qualifiers["translation"][0]
        else:
          continue
        rec = SeqRecord(Seq(feature.qualifiers["translation"][0], IUPAC.protein),
            id = "%s::%s::B%d:E%d:S%s" % (accessions, \
                feature.qualifiers["locus_tag"][0], \
                feature.location.start, \
                feature.location.end, \
                ("+" if (feature.location.strand > 0) else "-")), \
            description = desc, \
            name = feature.qualifiers["product"][0])
        rec.annotations["start"] = feature.location.start
        rec.annotations["end"] = feature.location.end
        rec.annotations["strand"] = feature.location.strand
        yield rec

p = argparse.ArgumentParser(description = ("Convert a Genbank-formatted file" \
  "to a FASTA-formatted file with consistent gene info"))
p.add_argument("gbfile", type = str, nargs = "+", \
    help = "input file")
p.add_argument("-z", dest="compressed", default = False, action = "store_true", \
    help = "uncompress file (gz)")
args = p.parse_args()

if (args.compressed):
  fh = gzip.open(args.gbfile[0], "rt")
else:
  fh = open(args.gbfile[0], "rt")
for record in SeqIO.parse(fh, "genbank"):
  accessions = ";".join(record.annotations["accessions"])
  features = [f for f in feature_gen(record.features, accessions)]
  features.sort(key = lambda x: x.annotations["start"])
  for (fn, f) in enumerate(features):
    f.id = "%s:N%d" % (f.id, fn)
    SeqIO.write(f, sys.stdout, "fasta")
fh.close()
