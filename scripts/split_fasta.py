#!/usr/bin/env python

import sys, os
import click
import pandas as pd
from Bio import SeqIO

@click.command()
@click.option('--fasta', '-f', type=str)
@click.option('--table', '-t', type=str)
@click.option('--column', '-c', type=str)
@click.option('--output-match', '-A', type=str, default=None)
@click.option('--output-unmatch', '-B', type=str, default=None)
@click.option('--drop-cutoff', '-d', type=float, default=1e-10)
@click.option('--drop-column', '-C', type=str)
@click.option('--add-genome/--no-add-genome', '-g/-G', default=False)
@click.option('--delimiter', '-D', type=str, default="____")
def split(fasta, table, column, output_match, output_unmatch, \
          drop_cutoff, drop_column, add_genome, delimiter):
    tbl = pd.read_csv(table, delimiter='\t', encoding='utf-8')
    if add_genome:
        new_ids = [n[0] + "____" + n[1] for n in zip(tbl[column], tbl["Genome"])]
        tbl[column] = new_ids
    all_match_ids = set(tbl[column])
    top_match_ids = set(tbl[tbl[drop_column] <= drop_cutoff][column])
    with open(fasta, 'r') as fh, \
         open(output_match, 'w') as omh, \
         open(output_unmatch, 'w') as onh:
        for record in SeqIO.parse(fh, 'fasta'):
            nm = record.name
            if add_genome:
                n = nm
            else:
                n = nm.split(delimiter)[0]
            d = record.description
            if n in all_match_ids:
                if n in top_match_ids:
                    SeqIO.write(record, omh, "fasta")
                else:
                    sys.stderr.write("Excluded ambiguous match %s\n" % n)
            else:
                SeqIO.write(record, onh, "fasta")

if __name__ == '__main__':
  split()

