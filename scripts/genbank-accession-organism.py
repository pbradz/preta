#!/usr/bin/python

import os
import sys
import argparse

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import gzip
import operator

p = argparse.ArgumentParser(description = ("Extract accessions and organism " \
    "from a Genbank-formatted file"))
p.add_argument("gbfile", type = str, nargs = "+", \
    help = "input file")
p.add_argument("-z", dest="compressed", default = False, action = "store_true", \
    help = "uncompress file (gz)")
args = p.parse_args()

if (args.compressed):
  fh = gzip.open(args.gbfile[0], "rt")
else:
  fh = open(args.gbfile[0], "rt")
for record in SeqIO.parse(fh, "genbank"):
  accessions = record.annotations["accessions"]
  organism = record.annotations["organism"]
  for a in accessions:
    sys.stdout.write("%s\t%s\n" % (a, organism))
fh.close()
