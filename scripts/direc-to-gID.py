#!/usr/bin/env python

import os
import gzip
import click
import sys
import csv

@click.command()
@click.option('--midas_dir', '-m',
              default='/home/pbradz/projects/epistasis/plr/data/midas/midas_db_v1.0',
              help='Directory containing a directory of genome clusters')
def run(midas_dir):
    sys.stderr.write("Finding genome clusters...\n")
    gclust_dir = os.path.join(midas_dir, 'genome_clusters')
    gclust = [d for d in os.listdir(gclust_dir) if
              os.path.isdir(os.path.join(gclust_dir, d))]
    gclust_path = [os.path.join(gclust_dir, g) for g in gclust]
    genomes = [None] * len(gclust)
    sys.stderr.write("{} genome files found...\n".format(len(gclust)))
    sys.stderr.write("Reading genome files...\n")
    for (n, gp) in enumerate(gclust_path):
#        print(os.path.join(gp, 'genomes.txt.gz'))
        try:
            with gzip.open(os.path.join(gp, 'genomes.txt.gz'), 'rt') as fh:
                reader = csv.DictReader(fh, delimiter='\t')
                genomes[n] = [row['genome_id'] for row in reader]
        except Exception as e:
            sys.stderr.write("{}\n".format(e))
            continue
    sys.stderr.write("Writing output...")
    writer = csv.writer(sys.stdout, delimiter='\t')
    writer.writerow(['midas_id', 'genome_id'])
    for (n, g) in enumerate(gclust):
        if genomes[n] is not None:
            for gid in genomes[n]:
                writer.writerow([g, gid])


if __name__ == "__main__":
    run()
