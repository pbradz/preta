#!/usr/bin/env python

## Keep reads from a fastq file matching input (multi-)HMMs

import os, sys, subprocess, copy
import csv
import re
import glob
import click
import tempfile
import gzip
import tarfile
import io
from Bio import SearchIO, SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna, generic_protein
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from Bio.Seq import reverse_complement, transcribe, back_transcribe, translate

## Function and class definitions

def HitsPassingCutoff(filename, cutoff):
    rparse = SearchIO.parse(filename, "hmmer3-tab")
    resultlist = [FilterHits(r, cutoff) for r in rparse]
    return(resultlist)

def FilterHits(result, cutoff):
  return([h for h in result.hits if h.evalue <= cutoff])

def GetMarkers(hmm):
    names = set()
    with open(hmm, 'r') as fh:
        for f in fh:
            m = re.match('NAME +(.*)$', f)
            if not m is None:
                names.add(m.group(1))
    return(list(names))

# Note: also handles case where this is an actual buffer
def MaybeGzipOpen(f):
    if isinstance(f, io.BufferedIOBase):
        return(io.TextIOWrapper(f))
    if f.endswith(".gz"):
        return(gzip.open(f, 'rt'))
    else:
        return(open(f))

def SixFrameTranslate(fastq, outfile, encoding, tbl='Standard'):
    with MaybeGzipOpen(fastq) as ifh:
        with open(outfile, "w") as ofh:
            for title, seq, qual in FastqGeneralIterator(ifh):
                tsplit = title.split(" ")
                seqs = [seq] * 6
                for i in range(0, 3):
                    seqs[i] = seqs[i][i:]
                for i in range(3, 6):
                    seqs[i] = reverse_complement(seqs[i])[(i-3):]
                for i in range(0, 6):
                    l = len(seqs[i])
                    if (l % 3) > 0:
                        seqs[i] = seqs[i] + ('N' * (3 - (l % 3)))
                    seqs[i] = translate(seqs[i], tbl, stop_symbol='X')
                for (n, s) in enumerate(seqs):
                    nsplit = tsplit.copy()
                    nsplit[0] = nsplit[0] + ("_%d" % (n + 1))
                    ofh.write(">%s\n%s\n" % (" ".join(nsplit), s))
    ifh.close()
    return

## Program body

@click.command()
@click.option('--fastq-file', '-f', 'fastq_file',
              help='Original FASTQ file')
@click.option('--transeq-file', '-F', 'transeq_file', default=None,
              help='Sixframe-translated FASTQ file (optional)')
@click.option('--trans-table', '-T', 'trans_table', default='Standard',
              help='Codon table to use for translation (name or NCBI ID)')
@click.option('--marker-file', '-p', 'marker_file', default='phyeco.hmm',
              help='Multi-HMM file containing markers/genes to find')
@click.option('--output-file', '-o', 'output_file', default='filtered.fq',
              help='Where to store filtered FASTQ (file, or for tarball, directory)')
@click.option('--tmp-dir', '-t', 'tmp_dir', default='/tmp/',
              help='Directory to store temporary HMMER3 files')
@click.option('--verbose', '-v', 'verbose', default=0,
              help='Set verbosity, where 0 = none, 1 = monitor, 2 = debug')
@click.option('--cutoff', '-c', 'cutoff', default=1.00,
              help='Set e-value cutoff for results')
@click.option('--encoding', '-e', 'encoding', default='fastq-sanger',
              help='Set type of FASTQ file (see BioPython documentation)')
@click.option('--out-type', '-O', 'out_type', default='fastq',
              type=click.Choice(['fastq', 'sixframe']),
              help='Output type: original fastq reads or sixframe-translated')
@click.option('--translate-program', '-X', 'translate_program', default='internal',
              type=click.Choice(['internal', 'transeq']),
              help='Output type: original fastq reads or sixframe-translated')
@click.option('--input-type', '-I', 'input_type', default='fastq',
              type=click.Choice(['fastq', 'tarball']),
              help='Is input a single FASTQ file or a (compressed) tarball?')
@click.option('--file-regex', '-M', 'file_regex', default='.*\\..\\.fastq',
              help='If input is a tarball, which files should be processed?')
def run(fastq_file, marker_file, output_file, transeq_file,
        trans_table, tmp_dir, verbose, cutoff, encoding, out_type,
        translate_program, input_type, file_regex):
    markers = GetMarkers(marker_file)
    ml = len(markers)
    transeq_file_h = None

    # Figure out whether dealing with one file or many
    if input_type=='fastq':
        input_files = [fastq_file]
        output_files = [output_file]
    elif input_type=='tarball':
        Th = tarfile.open(fastq_file)
        valid_inputs=[i for i in Th.getnames() if re.match(file_regex, i)]
        input_files=[Th.extractfile(i) for i in valid_inputs]
        newext = '.gz'
        if out_type=='fastq': newext = ".fastq.gz"
        elif out_type=='sixframe': newext = ".fasta.gz"
        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        output_files=[os.path.join(os.path.dirname(output_file), \
                                   os.path.basename(re.sub("\\.fastq", newext, i))) \
                      for i in valid_inputs]

    if verbose > 1: click.echo(str(zip(input_files, output_files)))

    for (infile, outfile) in zip(input_files, output_files): # iterate through list
    # Generate 6-frame translated file
        if not transeq_file:
            if verbose > 0: click.echo("Sixframe translating...")
            transeq_file_h=tempfile.mkstemp(dir=tmp_dir)
            transeq_file=transeq_file_h[1]
            if translate_program == "transeq" and input_type=='fastq':
                os.system("transeq -sequence %s -outseq %s -frame 6 -clean" %
                            (infile, transeq_file))
            else:
                SixFrameTranslate(infile, transeq_file, encoding, tbl=trans_table)

        hmm_outfile=tempfile.mkstemp(dir=tmp_dir)

        if verbose > 0: click.echo("Performing HMM search against sixframe...")
        os.system("hmmsearch --cpu 1 --tblout=%s %s %s > /dev/null" %
                    (hmm_outfile[1], marker_file, transeq_file))
        rlist = HitsPassingCutoff(hmm_outfile[1], cutoff)

        if verbose > 1: click.echo("  ...%d/%d markers found" % (len(rlist), ml))
        for (n, r) in enumerate(rlist):
            if verbose > 1:
                click.echo("  ==> marker %d: %d copies passed" % (n, len(r)))
        if verbose > 1: click.echo(str(rlist))
        matches = set()
        for hits in rlist:
            for hit in hits:
                if out_type == "fastq":
                    matches.add(re.sub("(.*)_.*", "\\1", hit.id))
                elif out_type == "sixframe":
                    matches.add(hit.id)
        if verbose > 1: click.echo(str(matches))
        if len(matches) == 0:
            click.echo("No matches found, creating empty result file")
            open(outfile, "a").close()
            return
        if out_type == "fastq":
            if verbose > 0: click.echo("Filtering original sequences...")
            with MaybeGzipOpen(infile) as ifh:
                with open(outfile, "w") as ofh:
                    for record in SeqIO.parse(ifh, encoding):
                        if verbose > 2: click.echo(record.id)
                        if record.id in matches:
                            if verbose > 1: click.echo("found match %s" % record.id)
                            SeqIO.write(record, ofh, encoding)
        elif out_type == "sixframe":
            if verbose > 0: click.echo("Filtering sixframe sequences...")
            with MaybeGzipOpen(transeq_file) as ifh:
                #with gzip.open(outfile, "w") as ofh:
                with gzip.open(outfile, "wt") as ofh:
                    for record in SeqIO.parse(ifh, 'fasta'):
                        if verbose > 2: click.echo(record.id)
                        if record.id in matches:
                            if verbose > 1: click.echo("found match %s" % record.id)
                            SeqIO.write(record, ofh, 'fasta')

        try:
            os.close(hmm_outfile[0])
        except:
            if verbose > 0: click.echo("HMM output file already closed", err=True)
            pass
        if transeq_file_h:
            try:
                os.close(transeq_file_h[0])
            except:
                if verbose > 0: click.echo("transeq file already closed", err=True)
                pass

    if input_type=='tarball':
        Th.close()

if __name__ == '__main__':
    run()
