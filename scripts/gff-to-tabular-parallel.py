#!/usr/bin/env python

from __future__ import print_function

import os
import re
import sys
import argparse
import gffutils
import functools
from joblib import Parallel, delayed

import gzip
import operator

def DummyGFF(ngff, gffF, args):
  print(ngff)
  gffdb = gffutils.create_db(gffF,
      ':memory:',
      sort_attribute_values=True,
      merge_strategy='create_unique')
  cds = gffdb.features_of_type("CDS", order_by=("seqid", "start"))
  features = gffdb.features_of_type(["gene", "pseudogene"],
      order_by=("seqid", "start"))
  regions = gffdb.features_of_type("region")

def ProcessGFF(ngff, gffF, args):
  print(gffF)
  if args.islist and args.output:
    print("processing file: %s" % gffF, file = sys.stderr)
  bn, ext = os.path.splitext(os.path.basename(gffF))
  if args.output:
    while ".gff" in bn:
      bn, ext = os.path.splitext(os.path.basename(bn))
    gfftab = "%s.tab" % (bn)
    os.makedirs(args.outdir, exist_ok=True)
    if args.divided:
      od = os.path.join(args.outdir, subdirs[ngff])
      os.makedirs(od, exist_ok=True)
      outpath = os.path.join(od, gfftab)
    else:
      outpath = os.path.join(args.outdir, gfftab)
    if args.skip and os.path.exists(outpath):
      if os.path.getsize(outpath) > 0:
        sys.stderr.write("...skipping (already exists)\n")
        return
    outfh = open(outpath, 'w')
  else:
    outfh = sys.stdout
  try:  
    gffdb = gffutils.create_db(gffF,
        ':memory:',
        sort_attribute_values=True,
        merge_strategy='create_unique')
  except:
    sys.stderr.write("...skipping (couldn't open GFF)\n")
    return
  cds = gffdb.features_of_type("CDS", order_by=("seqid", "start"))
  features = gffdb.features_of_type(["gene", "pseudogene"],
      order_by=("seqid", "start"))
  regions = gffdb.features_of_type("region")
  seq_to_taxon = dict()
  for r in regions:
    if 'Dbxref' in r.attributes.keys():
      try:
        taxids = [re.match('.*taxon:([^,]*)', x).group(1) for x in \
          r.attributes.get("Dbxref")]
        seq_to_taxon[r.seqid] = ",".join(taxids)
      except AttributeError:
        pass
    else:
      return
  taxonflag = False
  if len(seq_to_taxon.keys()) == 0 and not args.mag:
    sys.stderr.write("Warning: no taxon identifiers found in the entire GFF\n")
    taxonflag = True
  print(("\t".join(['protein_id',
    'sequence_id',
    'gene_id',
    'gene_name',
    'taxon',
    'start',
    'stop',
    'feature_number',
    'strand'])), file=outfh)
  pid_marker = 'ID' if args.mag else 'protein_id'
  if args.mag:
    operate_over = enumerate(cds)
  else:
    operate_over = enumerate(features)
  for (n, t) in operate_over:
    sid = t.seqid
    if not (args.mag or taxonflag):
      if sid in seq_to_taxon.keys():
        tid = seq_to_taxon[sid]
      else:
          tid = "NA"
    else:
      tid = bn
    try:
      if args.mag:
        pids = t.attributes.get(pid_marker)
      else:
        plist = [set(cds.attributes.get(pid_marker)) \
            for cds in gffdb.children(t.id) \
            if cds.featuretype == 'CDS']
        pset = functools.reduce(set.union, plist)
        pids = ",".join(pset)
    except TypeError:
      pids = "NA"
    try:
      name = ";".join(t.attributes.get('Name'))
    except TypeError:
      name = "NA"
    print(("\t".join([
      ("%s" % pids),
      ("%s" % sid),
      ("%s" % t.id),
      ("%s" % name),
      ("%s" % tid),
      ("%d" % t.start),
      ("%d" % t.stop),
      ("%d" % n),
      ("%s" % t.strand)])), 
      file = outfh)
  if args.output:
    outfh.close()


if __name__ == "__main__":

  p = argparse.ArgumentParser(description = ("Convert a GFF file" \
    "to a tab-delimited file that can be used to more-easily interpret" \
    " HMMsearch results"))
  p.add_argument("gfffile", type = str, \
      help = ".gff input file or list of paths to .gffs")
  p.add_argument("-l", dest="islist", default = False, action = "store_true", \
      help = "passes in a list of gffs instead of a single gff; useful if " +
      "startup overhead is high")
  p.add_argument("-D", dest="divided", default = False, action = "store_true", \
      help = "set this if the gff list is divided into subdirectories")
  p.add_argument("-o", dest="output", default = False, action = "store_true", \
      help = "output results to .tab files instead of stdout")
  p.add_argument("-d", dest="outdir", default = "tabs", help = \
      "change output directory [default = ./tabs/]")
  p.add_argument("-s", dest="skip", default = False, action = "store_true", \
      help = "skip wherever there's an output file of more than 0 bytes")
  p.add_argument("-m", dest="mag", default = False, action = "store_true", \
      help = "indicates these GFFs are from MAGs")
  p.add_argument("-j", dest="cores", type = int, \
      help = "use this many cores")

  #p.add_argument("-z", dest="compressed", default = False, action = "store_true", \
  #    help = "uncompress file (gz)")
  args = p.parse_args()

  if args.islist:
    if args.divided:
      with open(args.gfffile, 'rt') as fh:
        [subdirs, gfflist] = map(list, zip(*[line[:-1].split("\t") for line in fh]))
    else:
      with open(args.gfffile, 'rt') as fh:
        gfflist = [line[:-1] for line in fh]
  else:
    gfflist = [args.gfffile]


  Parallel(n_jobs=args.cores)(delayed(ProcessGFF)(ngff, gffF, args) for (ngff,
    gffF) in enumerate(gfflist))


