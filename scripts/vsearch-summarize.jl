using ArgParse, Query, DataFrames, Statistics, Glob

z(x) = (x .- mean(skipmissing(x))) ./ std(skipmissing(x))

function analyze(opts)
    output_dir = mkpath(opt["output_dir"])
    glob("*_quantify.txt", opt["result_dir"])


function main()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--result_dir", "-r"
            help = "path to vsearch results"
            arg_type = String
        "--mc_dir", "-m"
            help = "path to microbecensus results"
            type = String
        "--rc_dir", "-c"
            help = "path to read count results"
            type = String
        "--output_dir", "-o"
            help = "output directory"
        type = String
        "--min_len", "-L"
            type = Int
            help = "minimum length of alignment"
        default = 20
        "--min_bitscore",

    end
    parsed = parse_args(s)
    analyze(parsed)
end

main()
