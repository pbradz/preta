#!/usr/bin/python

import sys, os
import csv
import re
import glob
from Bio import SearchIO
from ete3 import NCBITaxa

EThresh = 1e-2
GenomeFile = "/pollard/home/pbradz/projects/preta/data/hmm-output/genomes.txt"
NamesFile = "/pollard/home/pbradz/data/ncbi/names.dmp"
TabsDir = "/pollard/home/pbradz/projects/preta/data/parsed-gffs/tabs"
HMMOutDir = "/pollard/home/pbradz/projects/preta/data/hmm-output/"
ResultDir = sys.argv[1]

def CommentStripper(iterator):
  for line in iterator:
    if line.startswith('#'):
      continue
    if not line.strip():
      continue
    yield line


def HMMParse(filename):
  hits = dict()
  results = SearchIO.read(filename, "hmmer3-tab")
  for result in results:
    desc = result.description
    e_val = float(result.evalue)
    parsed = re.findall("(\[[^\]]*=[^\]]*\])", desc)
    for p in parsed:
      m = re.match("\[(.*)=(.*)\]", p)
      if not m == None:
        if (m.group(1) == "protein_id"):
          hits[m.group(2)] = e_val
  return(hits)

def NCBIParse(filename):
  taxa = dict()
  with open(filename, 'rb') as fh:
    lastorg = "NA" 
    for line in fh:
      rem_end_delim = re.sub("\t\|\n", "", line)
      row = rem_end_delim.split("\t|\t")
      org = str(row[0])
      if row[3] == "scientific name":
        taxa[org] = row[1]
      if (not (org == lastorg)) and \
          (not(lastorg == "NA")) and \
          (not taxa.has_key(lastorg)):
        taxa[lastorg] = "node #%s" % org
      lastorg = org
  return(taxa)

def GFFParse(filename):
  proteins = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      if not row["protein_id"] == "None":
        pid = row["protein_id"]
        proteins[pid] = dict()
        proteins[pid]["taxon"] = row["taxon"]
        proteins[pid]["gene_number"] = int(row["gene_number"])
        proteins[pid]["strand"] = row["strand"]
  return(proteins)

def oldTaxonomyParse(filename, taxonomy):
  species_to_midas = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      gid = str(row["taxon_id"])
      species = MakeSpecies(gid, taxonomy)
#      lineage = taxonomy.get_lineage(gid)
#      ranks = taxonomy.get_rank(gid)
#      species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
#          if i == "species"]
#      print >> sys.stderr, "%s == %s == %s" % (gid, species, row["species_id"])
      if (len(species) == 1):
        species_to_midas[str(species[0])] = str(row["species_id"])
  return(species_to_midas)

def TaxonomyParse(filename):
  species_to_midas = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      sn = str(row["species_name"])
      si = str(row["species_id"])
      species_to_midas[sn] = si
  return(species_to_midas)

def SpeciesInfoParse(filename):
  midas_names = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile), delimiter='\t')
    for row in reader:
      midas_names[row["species_id"]] = row["species_name"]
  return(midas_names)

def PhenotypeParse(filename):
  phenotype = dict()
  with open(filename, 'rb') as csvfile:
    reader = csv.DictReader(CommentStripper(csvfile))
    for row in reader:
      phenotype[row["ID"]] = float(row["logit.Prevalence"])
  return(phenotype)

def MakeSpecies(taxon, taxonomy):
  lineage = taxonomy.get_lineage(taxon)
  ranks = taxonomy.get_rank(lineage)
  species = [ranks.keys()[n] for (n, i) in enumerate(ranks.values()) \
      if i == "species"]
  return(species)


# read genomes file
print >> sys.stderr, "loading taxonomy and prevalence"
ncbiT = NCBITaxa()
taxonomy = oldTaxonomyParse(os.path.join(HMMOutDir,
  "taxonomy-with-sp.csv"), ncbiT)
print(dict(taxonomy.items()[0:10]))
invtax = dict([reversed(i) for i in taxonomy.items()])
print(dict(invtax.items()[0:10]))
