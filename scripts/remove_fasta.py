#!/usr/bin/env python

import sys, os
import click
import csv
import re
import glob
import copy
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

@click.command()
@click.option('--infile', '-i', type=str)
@click.option('--outfile', '-o', type=str, default=None)
@click.option('--keep/--no-keep', '-k', default=False)
@click.option('--word', '-w')
def process(infile, outfile, keep, word):
  if outfile:
    oh = open(outfile, 'w')
  else:
    oh = sys.stdout
  wr = re.compile(word)
  with open(infile, 'r') as fh:
    for record in SeqIO.parse(fh, 'fasta'):
      n = record.name
      d = record.description
      matched = re.search(wr, d)
      if not keep:
        matched = not matched
      if matched:  
        SeqIO.write(record, oh, "fasta")
  oh.close()    

if __name__ == '__main__':
  process()
