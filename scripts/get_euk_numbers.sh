#!/bin/bash

find /pollard/data/eukaryote_and_archaeal_genomes/refseq/ -name "*protein*gz" -exec sh -c 'F={}; echo $F; echo $F     $(zgrep "^>" $F | wc -l) >> euk_genome_number.txt' \;
Rscript -e "cat(sum(read.table('euk_genome_number.txt', sep = ' ', stringsAs=FALSE)[,2]))" > ../../../data/processed/total_euk_number.txt
