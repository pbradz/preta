#!/bin/bash

echo processing $1...
y=`basename $1 .gff.gz`
z=$y.tab
python /pollard/home/pbradz/projects/preTA/scripts/gff-to-tabular.py -s $1 -o ./tabs/$z
