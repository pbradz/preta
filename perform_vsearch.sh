#!/bin/env bash

snakemake --jobs 50 --local-cores 4 -p --cluster 'qsub -wd $PWD/cluster_log/ -l mem_free=1G -l h_rt=48:00:00 -l hostname="!(qb3-ad*)&!msg-ogpu13" -pe smp 4 -V ' all_count_reads all_microbecensus_q all_vsearch
