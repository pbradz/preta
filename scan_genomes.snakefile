
### List, count, and parse RefSeq gff files:

rule prep_gffs:
    input: expand(["data/processed/parsed-gffs/cds_count_{gfftype}.txt", \
                   "data/processed/parsed-gffs/all-{gfftype}-gffs", \
                   "data/processed/parsed-gffs/divided-{gfftype}-gffs"], \
                  gfftype=GFFTYPES)

# For MAGs, we need to make sure we're not using low-quality genomes
rule mag_gffs_to_keep:
    input: "data/raw/IGG_genome_info_206581.txt.gz"
    output: "data/processed/parsed-gffs/high-medium-mags.txt"
    shell: "zless {input} | awk 'NR > 1 {{ if ($6 == \"high\" || $6 == \"medium\") print $1 \".gff\" }}' > {output}"
   
rule list_gffs:
    output: "data/processed/parsed-gffs/all-{gfftype}-gffs-unfiltered"
    run:
        gffdir = config["gffs"][wildcards.gfftype]["dir"]
        print(gffdir)
        shell("find {gffdir} -name '*.gff*' ! -name \"*Viral*\" > {output}")

rule list_gffs_filtered:
    input:
        unf="data/processed/parsed-gffs/all-{gfftype}-gffs-unfiltered",
        keep=ancient("data/processed/parsed-gffs/high-medium-mags.txt")
    output: "data/processed/parsed-gffs/all-{gfftype}-gffs"
    run:
        if wildcards.gfftype=="mags":
            shell("scripts/filter_files.R -r {input.keep} -g {input.unf} -o {output}")
        else:
            shell("cp {input.unf} {output}")

# Subdivide (to avoid file system stress)
rule subdivide_gffs:
    input: "data/processed/parsed-gffs/all-{gfftype}-gffs"
    output: "data/processed/parsed-gffs/divided-{gfftype}-gffs"
    run:
        is_mags = config["gffs"][wildcards.gfftype]["mags"]
        if is_mags:
            NFILES = int(config["nfiles"])
            shell("cat {input} | scripts/dirdivide.py --nfiles={NFILES} --layers=2 > {output}")
        else:
            NDIRS = int(config["ndirs"])
            shell("cat {input} | scripts/dirdivide.py --ndirs={NDIRS} --loop=True > {output}")

rule subdivide_cds:
    input: "data/processed/parsed-gffs/divided-{gfftype}-gffs"
    output: "data/processed/parsed-gffs/divided-{gfftype}-cds"
    run:
        genome = config["gffs"][wildcards.gfftype]["gff_name"]
        cds = config["gffs"][wildcards.gfftype]["cds_name"]
        shell("cat {input} | sed 's/{genome}/{cds}/g' > {output}")

def name_of_genome(genome):
    genomename = re.sub('\.fna\.gz', '', os.path.basename(genome))
    return(genomename)

def get_divided(filename, wildcards):
    target = make_midas_outf([wildcards.subdir, wildcards.genome])
    with open(filename, 'r') as fh:
        for line in fh:
            row = line[:-1].split('\t')
            outfile = make_midas_outf(row)
            if (outfile == target):
                return(row[1])
    return None

DividedCDSDict = dict()
if os.path.exists('data/processed/parsed-gffs/divided-refseq-cds'):
    with open('data/processed/parsed-gffs/divided-refseq-cds', 'r') as fh:
        for line in fh:
            row = line[:-1].split('\t')
            infile = row[1]
            genome = name_of_genome(infile)
            DividedCDSDict[genome] = infile

rule list_genomes:
    input: 'data/processed/parsed-gffs/divided-{gfftype}-cds'
    output: 'data/processed/parsed-gffs/list-{gfftype}-genomes.txt'
    run:
        cds = config["gffs"][wildcards.gfftype]["cds_name"]
        protein = config["gffs"][wildcards.gfftype]["protein_name"]
        with open(str(output), 'w') as fhO:
            with open(str(input), 'r') as fhI:
                for line in fhI:
                    row = line[:-1].split("\t")
                    g = re.sub(cds, protein, row[1])
                    print(g)
                    if os.path.exists(g):
                        fhO.write(g + "\n")

rule count_cds:
    input: "data/processed/parsed-gffs/all-{gfftype}-gffs"
    output: "data/processed/parsed-gffs/cds_count_{gfftype}.txt"
    run:
        gffdir = config["gffs"][wildcards.gfftype]["dir"]
        gz = config["gffs"][wildcards.gfftype]["gzip"]
        if gz:
            grep = "zgrep"
        else:
            grep = "grep"
        shell("cat {input} | xargs -I{{}} sh -c '{grep} \"CDS\" {{}} | wc -l' > {output}")

rule sum_refseq_cds:
    input: "data/processed/parsed-gffs/cds_count_{gfftype}.txt"
    output: "data/processed/parsed-gffs/total_cds_{gfftype}.txt"
    run:
        shell("awk -F '\t' '{{sum += $1}} END {{print sum}}' '{input}' > {output}")

# These are super time consuming so we don't regenerate unless explicitly forced
rule run_hmms_indiv:
    input:
        gfflist=ancient("data/processed/parsed-gffs/divided-{gfftype}-gffs"),
        nseqfile=ancient("data/processed/parsed-gffs/total_cds_{gfftype}.txt"),
        preA=ancient("data/processed/hmm/preA-trimmed-automated1.hmm"),
        preT=ancient("data/processed/hmm/preT-trimmed-automated1.hmm")
    output:
        dir=directory("data/processed/hmm-output/{gfftype,\w+}"),
        touch="data/processed/hmm-output/{gfftype,\w+}/.touch"
    threads: 4
    run:
        is_mags = config["gffs"][wildcards.gfftype]["mags"]
        log = (not is_mags)
        wildcard = config["gffs"][wildcards.gfftype]["wildcard"]
        gdir = config["gffs"][wildcards.gfftype]["dir"]
        swapout = config["gffs"][wildcards.gfftype]["gff_name"]
        shell("""
        python scripts/run_hmms.py --wildcard={wildcard} \
                --gfflist={input.gfflist} --gdir={gdir} \
                --nseqfile={input.nseqfile} --outdir={output.dir} \
                --preT={input.preT} --preA={input.preA} --threads={threads} \
                --log={log} --swapout={swapout}
        """)
        shell("touch {output.touch}")

# Run HMMs against databases
rule run_hmms:
    input: expand("data/processed/hmm-output/{gfftype}", \
                   gfftype=GFFTYPES)

# Get statistics for report
rule n_indiv_results:
    input: ancient("data/processed/hmm-output/{gfftype}")
    output: "data/processed/hmm-summaries/{gfftype}/{marker}-{cutoff}-total.txt"
    shell:
        """
        find {input} -name "*{wildcards.marker}*.tab" -exec grep -v "^#" {{}} \; | \
             awk '{{ if ($5 <= {wildcards.cutoff}) print($5) }}' | \
             wc -l > \
             {output}
        """

rule generate_result_reports:
    input: expand("data/processed/hmm-summaries/{G}/{M}-1e-10-total.txt", \
                  G=GFFTYPES, M=["preT", "preA"])

