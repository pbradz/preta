# Manually processed data

Sequences were downloaded from NCBI Protein database (*-all-human.fa). The human DPYD is represented in both FASTA files.

The FASTA files were then aligned using T-Coffee [online](https://www.ebi.ac.uk/Tools/msa/tcoffee/) to produce the .clustalw format files included here.
