import os


# scan preT/preA HMMs against a list of fasta files to find preT/preA family members
rule hmm_scan:
    input:
        genomelist='data/processed/parsed-gffs/list-{gfftype}-genomes.txt',
        markerhmm='data/processed/hmm/preTA-trimmed-automated1.hmm'
    output:
        'data/processed/preTA-scan/{gfftype}/preT-trimmed-automated1',
        'data/processed/preTA-scan/{gfftype}/preA-trimmed-automated1'
    run:
        o_dir = os.path.dirname(str(output[0]))
        shell("python scripts/hmm_scanner.py "+
              "-g {input.genomelist} " +
              "-p {input.markerhmm} " +
              "-o {o_dir} " +
              "--return-all " +
              "-c 1e-10 " +
              "-v 1")

# rename to standard protein_____genome name format
rule hmm_process:
    input:
       hmm='data/processed/preTA-scan/{gfftype}/{marker}-trimmed-automated1'
    output:
       'data/processed/preTA-scan/{gfftype}/{marker}-filtered.fa'
    wildcard_constraints: gfftype="(?!all).*"
    run:
       shell("python scripts/rename_fasta_preTA.py " +
             "-i {input.hmm} " +
             "-o " +
             "{output} " +
             "-g " +
             "-c " +
             "-p " +
             "-n " +
             "--delimiter='____'")

# split the occurrences of preT and preA up by whether or not they appeared in an operon (processed-output-cut-1e-2.tab)
rule split_fastas:
     input:
         tbl=ancient('data/processed/hmm-summaries/{gfftype}/' +
                     'processed-output-cut-1e-2.tab'),
         fa=ancient('data/processed/preTA-scan/{gfftype}/' +
                    '{marker}-filtered.fa')
     output:
         match="data/processed/preTA-scan/split/" + \
             "{gfftype}/{marker}-preTA.fa",
         unmatch="data/processed/preTA-scan/split/" + \
             "{gfftype}/{marker}-non-preTA.fa"
     run:
         is_mags = config["gffs"][wildcards.gfftype]["mags"]
         if is_mags:
             shell("python scripts/split_fasta.py " +
                   "-f {input.fa} " +
                   "-t {input.tbl} " +
                   "--column=\"{wildcards.marker} protein ID\" " +
                   "--drop-column=\"{wildcards.marker} score\" " +
                   "--output-match=\"{output.match}\" " +
                   "--output-unmatch=\"{output.unmatch}\" " +
                   "--add-genome")
         else:
             shell("python scripts/split_fasta.py " +
                   "-f {input.fa} " +
                   "-t {input.tbl} " +
                   "--column=\"{wildcards.marker} protein ID\" " +
                   "--drop-column=\"{wildcards.marker} score\" " +
                   "--output-match=\"{output.match}\" " +
                   "--output-unmatch=\"{output.unmatch}\"")

# Get characteristics of input files
# (necessary to work across different fastq naming conventions)

def generic_get_samples(wc,
                        inputdir="data/raw/sb_samples",
                        outputdir="data/processed/vsearch_results/{dir}/",
                        suffix="_vsearch.txt",
                        paired=True):
    dirs = [f.path for f in os.scandir("data/raw/sb_samples") \
            if f.is_dir()]
    s = []
    for d in dirs:
        dir = path_components(d)[-1]
        # Catch FASTQ manifest standard files
        (samplename, pair) = glob_wildcards(d + \
            "/{sample}_{pair}.fastq.gz")
        if paired:
            s += [outputdir.format(dir=dir) + i[0] + "_" + \
                  i[1] + suffix for i in zip(samplename, pair)]
        else:
            s += [outputdir.format(dir=dir) + i + suffix for i in samplename]
        # Catch go_mgs-style files
        (samplename, pair) = glob_wildcards(d + \
            "/{sample}_R{pair}_001.fastq.gz")
        if paired:
            s += [outputdir.format(dir=dir) + i[0] + "_" + \
                  i[1] + suffix for i in zip(samplename, pair)]
        else:
            s += [outputdir.format(dir=dir) + i + suffix for i in samplename]
        # Catch non-paired files and add pair 1 automatically
        (samplename) = glob_wildcards(d + \
            "/{sample,[^_.]+}_fastq.gz")
        if paired:
            s += [outputdir.format(dir=dir) + i[0] + "_1" + \
                  suffix for i in samplename[0]]
        else:
            s += [outputdir.format(dir=dir) + i + suffix for i in samplename[0]]
    return(list(set(s)))


def get_mc_samples(wc): # uncomment if running on cluster
    return(generic_get_samples(wc,
                               outputdir="data/processed/microbecensus/{dir}/",
                               suffix="-census.txt",
                               paired=False))

def get_count_samples(wc): # uncomment if running on cluster
    return(generic_get_samples(wc,
                               outputdir="data/processed/readcount/{dir}/",
                               suffix="-count.txt",
                               paired=False))
def vsearch_samples(wc):
    return(generic_get_samples(wc,
                               outputdir="data/processed/vsearch_results/{dir}/",
                               suffix="_vsearch.txt"))

# Account for variability in which files are present and how they're named
# This should force renaming for unpaired and non-standardly-named files
def pair_sample_dyn(wc):
    paths1 = [f"data/raw/sb_samples/{wc.dir}/{wc.samplename}.fastq.{i}.gz" \
              for i in [1, 2]]
    paths2 = [f"data/raw/sb_samples/{wc.dir}/{wc.samplename}_{i}.fastq.gz" \
              for i in [1, 2]]
    test1 = [os.path.exists(p) for p in paths1]
    test2 = [os.path.exists(p) for p in paths2]
    if all(test1) or all(test2):
        return(paths2)
    possible = glob.glob(f"data/raw/sb_samples/{wc.dir}/{wc.samplename}*")
    if len(possible) > 0:
        remaining = possible[0]
    else:
        remaining = f"data/raw/sb_samples/{wc.dir}/{wc.samplename}_1.fastq.gz"
    return(remaining)

# Run MicrobeCensus to get (# genome equivalents) per sample 

rule all_microbecensus_q:
    input: get_mc_samples

rule microbecensus_q:
    threads: 1
    input: pair_sample_dyn
    output:
        "data/processed/microbecensus/{dir}/{samplename}-census.txt"
    run:
        # Handle exceptions by touching empty file since MicrobeCensus can fail on weird samples
        # and we don't want to hold up the entire pipeline by waiting until they're ID'd
        print(input)
        if len(input) > 1:
            shell("run_microbe_census.py {input[0]},{input[1]} {output} || touch {output}")
        else:
            shell("run_microbe_census.py {input} {output} || touch {output}")

# count total reads per sample

rule all_count_reads:
    input: get_count_samples

rule count_reads:
    threads: 1
    input: pair_sample_dyn
    output: "data/processed/readcount/{dir}/{samplename}-count.txt"
    run:
        if (len(input) == 1):
            shell("vsearch " +
                  "--threads {threads} " +
                  "--fastx_filter {input} " +
                  "--fastaout - | " +
                  "wc -l > {output}")
        else:
            inputs = " ".join(input)
            shell("zcat {inputs} | " +
                  "vsearch --threads {threads} " +
                  "--fastx_filter - " +
                  "--fastaout - | " +
                  "wc -l > {output}")

# snakemake -j 50 -p --cluster 'qsub -wd $PWD/cluster_log/ -l mem_free=2G -l \
# h_rt=24:00:00 -l hostname="!(qb3-ad*)&!msg-ogpu13" -V ' \
# all_count_reads all_microbecensus_q

# snakemake -j 50 -p --cluster 'qsub -wd $PWD/cluster_log/ -l mem_free=2G -l \
# h_rt=48:00:00 -l hostname="!(qb3-ad*)&!msg-ogpu13" -V ' \
# all_count_reads all_microbecensus_q

# Create databases

# Helper script to put names of files in organized table for results-to-regions.R
rule prepare_genome_tables:
    input: "data/processed/parsed-gffs/divided-{gff}-genomes"
    output: "data/processed/parsed-gffs/genome-table-{gff}"
    run:
        indir = config['gffs'][wildcards.gff]["dir"]
        tabdir = f"data/processed/parsed-gffs/tabs/{wildcards.gff}/"
        ext = config['gffs'][wildcards.gff]["genome_name"]
        shell("Rscript scripts/prepare_genome_tbl.R " +
              "-g {input} " +
              "-f {indir} " +
              "-x {ext} " +
              "-t {tabdir} " +
              "-o {output}")

rule all_genome_tables:
    input:
        expand("data/processed/parsed-gffs/genome-table-{gff}",
              gff=GFFTYPES)

rule non_preTA_tables:
    input:
        expand("data/processed/preTA-scan/split/{{gff}}/{marker}-non-preTA.fa",
               marker=GENES)
    output: "data/processed/preTA-scan/split/{gff}/non-preTA.tsv"
    shell: """
        Rscript scripts/make_nonpreTA_tables.R \
          -a {input[0]} \
          -b {input[1]} \
          -d ____ \
          -o {output}
    """

rule all_non_preTA_tables:
    input:
        expand("data/processed/preTA-scan/split/{gff}/non-preTA.fa",
               gff=GFFTYPES, marker=GENES)

rule positives_to_intermediate_regions:
    threads: 4
    input:
        summary="data/processed/hmm-summaries/{gff}/processed-output-cut-1e-10.tab",
        tbl="data/processed/parsed-gffs/genome-table-{gff}"
    output:
        fasta="data/processed/nucleotide-regions/{gff}-positive-regions.fa",
        tbl="data/processed/nucleotide-regions/{gff}-positive-regions.tsv"
    run:
        gid_col = config["gffs"][wildcards.gff]["geneid_col"]
        cmd = """
            Rscript scripts/results_to_regions.R -r {input.summary} \
              -w {gid_col} \
              -g {input.tbl} \
              -c {threads} \
              -o {output.fasta} \
              -O {output.tbl} \
        """
        if wildcards.gff == "refseq": cmd += " -U "
        print(cmd)
        shell(cmd)

rule negatives_to_intermediate_regions:
    threads: 4
    input:
        nonpreTAs="data/processed/preTA-scan/split/{gff}/non-preTA.tsv",
        tbl="data/processed/parsed-gffs/genome-table-{gff}"
    output:
        fasta="data/processed/nucleotide-regions/{gff}-negative-regions.fa",
        tbl="data/processed/nucleotide-regions/{gff}-negative-regions.tsv"
    run:
        gid_col = config["gffs"][wildcards.gff]["geneid_col"]
        cmd = """
            Rscript scripts/results_to_regions.R -r {input.nonpreTAs} \
                -w {gid_col} \
                -g {input.tbl} \
                -c {threads} \
                -X \
                -o {output.fasta} \
                -O {output.tbl} \
        """
        # if wildcards.gff == "refseq": cmd += " -U "
        print(cmd)
        shell(cmd)

rule finish_regions:
    input:
        expand("data/processed/nucleotide-regions/{gff}-negative-regions.fa",
               marker=GENES, gff=GFFTYPES)
    output:
        "data/processed/nucleotide-regions/negative-regions.fa"
    shell:
        "cat {input} > {output}"

rule finish_regions_2:
    input:
        expand("data/processed/nucleotide-regions/{gff}-positive-regions.fa",
               gff=GFFTYPES)
    output:
        "data/processed/nucleotide-regions/positive-regions.fa"
    shell:
        "cat {input} > {output}"

rule finish_regions_3:
    input:
        expand("data/processed/nucleotide-regions/{gff}-positive-regions.tsv",
               gff=GFFTYPES)
    output:
        "data/processed/nucleotide-regions/positive-regions.tsv"
    run:
        # need to get rid of headers for the second and following input files
        shell("cat {input[0]} > {output}")
        for i in input[1:]:
            shell("tail -n +2 {i} >> {output}")

rule vclust_regions:
    threads: 4
    input:
        "data/processed/nucleotide-regions/{ftype}-regions.fa"
    output:
        centroids="data/processed/nucleotide-regions/vclust/{ftype,[^-]*}-regions-vclust-0_95.fa",
        clusters="data/processed/nucleotide-regions/vclust/{ftype,[^-]*}-vclust-0_95.uc"
    shell:
        "vsearch --cluster_fast {input} --id 0.95 --centroids {output.centroids} --uc {output.clusters} --threads {threads}"
        #"cd-hit-est -g 1 -c 0.95 -sc -sf -i {input} -o {output} -T {threads}"

rule concat_nucleotide_regions:
    input:
        expand("data/processed/nucleotide-regions/{ftype}-regions.fa",
               ftype=['positive', 'negative'])
    output:
        "data/processed/nucleotide-regions/all-regions.fa"
    shell: "cat {input} > {output}"

rule concat_clustered_regions:
    input:
        expand("data/processed/nucleotide-regions/vclust/{ftype}-regions-vclust-0_95.fa",
               ftype=['positive', 'negative'])
    output:
        "data/processed/nucleotide-regions/vclust/all-regions-concat-0_95.fa"
    shell: "cat {input} > {output}"

rule vsearch_rename_go: # rename troublesome GO samples
    input: "data/raw/sb_samples/{dir}/{sample}_R{pair}_001.fastq.gz"
    output: "data/raw/sb_samples/{dir}/{sample}_{pair,\d}.fastq.gz"
    shell: "mv {input} {output}"

rule vsearch_rename_single: # rename troublesome single-pair samples
    input: "data/raw/sb_samples/{dir}/{sample}.fastq.gz"
    output: "data/raw/sb_samples/{dir}/{sample,[^_]+}_1.fastq.gz"
    shell: "mv {input} {output}"

def get_rename_all_go(wc):
    (samplename, pair) = glob_wildcards("data/raw/sb_samples/" + \
            "/go_mgs/{sample}_R{pair,\d}_001.fastq.gz")
    s = ["data/raw/sb_samples/go_mgs/" + i[0] + "_" + i[1] + ".fastq.gz" for i in zip(samplename, pair)]
    return(list(set(s)))

rule vsearch_rename_all_go:
    input: get_rename_all_go


rule vsearch_generic_samples:
    threads: 4
    input:
        sample="data/processed/vsearch_filter/{dir}/{sample}_{pair}.fasta.gz",
        db=ancient("data/processed/nucleotide-regions/all-regions.fa")
    output: "data/processed/vsearch_results/{dir}/{sample}_{pair,\d}_vsearch.txt"
    shell: """
        vsearch --usearch_global {input.sample} --db {input.db} --id 0.97 \
        --maxaccepts 32 --maxrejects 128 \
        --userfields query+target+id+alnlen+mism+opens+qilo+qihi+tilo+tihi+evalue+bits \
        --userout {output} \
        --threads {threads} \
        || touch {output}
    """

rule vsearch_generic_gzip:
    input:
        "data/processed/vsearch_results/{dir}/{sample}_{pair}_vsearch.txt"
    output:
        "data/processed/vsearch_results/{dir}/{sample}_{pair,\d}_vsearch.txt.gz"
    shell:
        "gzip {input}"

rule vsearch_filter_samples:
    threads: 4
    input:
        sample="data/raw/sb_samples/{dir}/{sample}_{pair}.fastq.gz",
        db=ancient("data/processed/nucleotide-regions/vclust/all-regions-concat-0_95.fa")
    output: "data/processed/vsearch_filter/{dir}/{sample}_{pair,\d}.fasta"
    shell: """
        vsearch --fastx_filter {input.sample} --fastaout - | \
        vsearch --usearch_global - --db {input.db} --id 0.95 \
        --maxaccepts 1 --maxrejects 32 \
        --matched {output} \
        --threads {threads} \
        || touch {output}
    """

rule vsearch_filter_gzip:
    input:
        "data/processed/vsearch_filter/{dir}/{sample}_{pair}.fasta"
    output: "data/processed/vsearch_filter/{dir}/{sample}_{pair,\d}.fasta.gz"
    shell: "gzip {input}" 

def path_components(path):
    s = path.split(os.sep)
    return([i for i in s if i != ''])

def vsearch_samples(wc):
    suffix = "_vsearch.txt.gz"
    dirs = [f.path for f in os.scandir("data/raw/sb_samples") \
            if f.is_dir()]
    s = []
    for d in dirs:
        dir = path_components(d)[-1]
        # Catch FASTQ manifest standard files
        (samplename, pair) = glob_wildcards(d + \
            "/{sample}_{pair}.fastq.gz")
        s += [f"data/processed/vsearch_results/{dir}/" + i[0] + "_" + \
              i[1] + suffix for i in zip(samplename, pair)]
        # Catch go_mgs-style files
        (samplename, pair) = glob_wildcards(d + \
            "/{sample}.fastq.{pair}.gz")
        s += [f"data/processed/vsearch_results/{dir}/" + i[0] + "_" + \
              i[1] + suffix for i in zip(samplename, pair)]
        # Catch non-paired files
        (samplename) = glob_wildcards(d + \
            "/{sample,[^_.]+}_fastq.gz")
        if len(samplename[0]) > 0:
          s += [f"data/processed/vsearch_results/{dir}/" + i + "_1" + \
                suffix for i in samplename[0]]
    return(list(set(s)))

rule all_vsearch:
    input: vsearch_samples

def vsearch_crc_samples(wc):
    (samplename, pair) = glob_wildcards("data/raw/sb_samples/" + \
            "/crc/{sample}_{pair}.fastq.gz")
    s = ["data/processed/vsearch_results/crc/" + i[0] + "_" + \
            i[1] + "_vsearch.txt" for i in zip(samplename, pair)]
    return(list(set(s)))

def vsearch_specific_samples(wc, sdir):
    (samplename, pair) = glob_wildcards("data/raw/sb_samples/" + \
            "/" + sdir + "/{sample}_{pair,\d}.fastq.gz")
    s = ["data/processed/vsearch_results/" + sdir + "/" + i[0] + "_" + \
            i[1] + "_vsearch.txt.gz" for i in zip(samplename, pair)]
    return(list(set(s)))

rule crc_vsearch:
    input: lambda wc: vsearch_specific_samples(wc, "crc")

rule go_mgs_vsearch:
    input: lambda wc: vsearch_specific_samples(wc, "go_mgs")

rule benchmarks_vsearch:
    input: lambda wc: vsearch_specific_samples(wc, "benchmarks")

rule tumor_vsearch:
    input: lambda wc: vsearch_specific_samples(wc, "tumor")

rule quantify_vsearch:
    input: "data/processed/vsearch_results/{dir}/{smp}_{pair}_vsearch.txt.gz"
    output: "data/processed/vsearch_quantify/L{L}p{p}/{dir}/{smp}_{pair,\d}_quantify.txt"
    shell: """
        scripts/relative_blast.R \
          -f {input}             \
          -P nonpreTA           \
          -o {output}            \
          -L {wildcards.L}       \
          -p {wildcards.p}       \
          -V TRUE
    """

def vsearch_results(wc):
    dirs = [path_components(f.path)[-1] for f in \
            os.scandir("data/processed/microbecensus/") \
            if f.is_dir()]
    s = []
    for d in dirs:
        (samplename, pair) = glob_wildcards("data/processed/vsearch_results/" +
                                            d + "/{smp}_{pair}_vsearch.txt")
        s += [f"data/processed/vsearch_quantify/L50p90/{d}/" + i[0] + "_" + \
              i[1] + "_quantify.txt" for i in zip(samplename, pair)]
    return(list(set(s)))

rule all_quantify_vsearch:
    input: vsearch_results

def vsearch_specific_results(wc, sdir):
    (s, p) = glob_wildcards("data/processed/vsearch_results/" +  \
                            sdir + "/" + \
                            "{smp}_{pair}_vsearch.txt.gz")
    sl2 = ["data/processed/vsearch_quantify/" + \
           "L50p90/" + \
           sdir + "/" + \
           i[0] + "_" + i[1] + "_quantify.txt" \
          for i in zip(s, p)]
    return(list(set(sl2)))

rule crc_quantify_vsearch:
    input: lambda wc: vsearch_specific_results(wc, "crc")

rule tumor_quantify_vsearch:
    input: lambda wc: vsearch_specific_results(wc, "tumor")

rule benchmarks_quantify_vsearch:
    input: lambda wc: vsearch_specific_results(wc, "benchmarks")

rule go_mgs_quantify_vsearch:
    input: lambda wc: vsearch_specific_results(wc, "go_mgs")

def vsearch_rpkg_files(wc):
    dirs = [path_components(f.path)[-1] for f in \
            os.scandir("data/processed/microbecensus") \
            if f.is_dir()]
    s = []
    for d in dirs:
        s += [f"data/processed/vsearch_rpkg/L50p90/{d}"]
    return(list(set(s)))

rule rpkg_vsearch:
    input:
        r = "data/processed/vsearch_quantify/L50p90/{d}/",
        m = "data/processed/microbecensus/{d}/",
        c = "data/processed/readcount/{d}/",
        rh = "data/processed/vsearch_quantify/L50p90/{d}/coverage_report/retained_hits.tsv",
        s = "data/processed/nucleotide-regions/positive-regions.fa",
        st = "data/processed/nucleotide-regions/positive-regions.tsv"
    output: directory("data/processed/vsearch_rpkg/L50p90/{d}")
    shell: "scripts/vsearch-summarize.R -r {input.r} -m {input.m} -c {input.c} -o {output} -p FALSE -F {input.rh} -s {input.s}"

rule all_vsearch_rpkg:
    input: vsearch_rpkg_files

rule orf_coverage:
    input:
        q = "data/processed/vsearch_quantify/L50p90/{d}/",
        t = "data/processed/taxonomy/full-taxonomy-genomes.tsv",
        g = "data/raw/IGG_genome_info_206581.txt.gz"
    output:
        "data/processed/vsearch_quantify/L50p90/{d}/coverage_report/retained_hits.tsv"
    run:
        odir = os.path.dirname(str(output))
        shell("Rscript scripts/orf_coverage.R -q {input.q} -P TRUE \
                -o {odir} \
                -T {input.t} \
                -G {input.g}")

rule assemble_taxonomy:
    input: "data/raw/IGG_genome_info_206581.txt.gz"
    output: "data/processed/taxonomy/full-taxonomy-genomes.tsv"
    shell: "Rscript scripts/assemble_taxonomy.R"

# map species

rule iggsearch:
    input:
        "data/raw/sb_samples/{dset}/{samplename}_1.fastq.gz",
        "data/raw/sb_samples/{dset}/{samplename}_2.fastq.gz",
        ancient("data/raw/iggdb_v1.0.0")
    output:
        "data/processed/iggsearch/{dset}/{samplename}/species_profile.tsv"
    threads: 4
    run:
        o_dir = os.path.dirname(str(output[0]))
        db_dir = input[2]
        if db_dir.endswith(os.sep):
            db_dir = db_dir[:-1]
        if len(input) == 3:
           shell("PYTHONPATH=$PWD/scripts/iggsearch IGG_DB={db_dir} python " + \
                   "scripts/IGGsearch/run_iggsearch.py search " + \
                   "--m1 {input[0]} --m2 {input[1]} --outdir {o_dir} " + \
                   "--threads {threads}")
        else: 
            print(f"skipping unpaired sample {str(input)}; creating empty output")
            shell("touch {output}")

rule iggsearch_go:
    input: lambda wc: iggsearch_go_results("go_mgs", wc)

rule iggsearch_crc:
    input: lambda wc: iggsearch_go_results("crc", wc)

def iggsearch_go_results(dset, wc):
    (samplename, pair) = glob_wildcards("data/raw/sb_samples/" + dset + "/{samplename}_{pair}.fastq.gz")
    return([f"data/processed/iggsearch/{dset}/{s}/species_profile.tsv" for s in list(set(samplename))])

def iggsearch_resdir(dset, wc):
    (samplename, pair) = glob_wildcards("data/raw/sb_samples/" + dset + "/{samplename}_{pair}.fastq.gz")
    return([f'data/processed/iggsearch/{dset}/'] + [f"data/processed/iggsearch/{dset}/{s}/species_profile.tsv" for s in list(set(samplename))])

rule iggsearch_go_merged:
    input: lambda wc: iggsearch_resdir("go_mgs", wc)
    output: directory("data/processed/iggsearch_merged/go_mgs/")
    shell: """
    IGG_DB=$PWD/data/raw/iggdb_v1.0.0 scripts/IGGsearch/run_iggsearch.py \
            merge --outdir {output} --input {input[0]} --intype dir
    """

rule iggsearch_crc_merged:
    input: lambda wc: iggsearch_resdir("crc", wc)
    output: directory("data/processed/iggsearch_merged/crc/")
    shell: """
    IGG_DB=$PWD/data/raw/iggdb_v1.0.0 scripts/IGGsearch/run_iggsearch.py \
            merge --outdir {output} --input {input[0]} --intype dir
    """
