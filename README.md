Codebase for the phylogenetic analyses for "Drug resistant gut bacteria mimic a host mechanism for anticancer drug inactivation" (in review).

Structure: the Snakemake pipeline generates the inputs for the R scripts in `analyses/`. To run these, first source `00-analysis-functions.R` and then run the other R scripts in order.

Other command-line tools that will be necessary for the Snakemake pipeline:

 - [HMMER3](http://hmmer.org/)
 - [BURST](https://github.com/knights-lab/BURST)
 - [AMAS.py](https://github.com/marekborowiec/AMAS/)
 - [Clustal Omega](http://www.clustal.org/omega/)
 - [FastTree2](http://www.microbesonline.org/fasttree/)
 - [trimal](http://trimal.cgenomics.org/)

This pipeline depends on searching an HMM against a large number of genomes: [all complete bacterial RefSeq] genomes and the [IGGdb](https://github.com/snayfach/IGGdb) metagenome-assembled genomes (MAGs). After downloading these, edit the paths in `config.json` to point to the root of the directory tree where the .gff and .faa files can be found.

```
.
+- data/                   # Data having to do with the project
|  +- raw/                 #   Immutable, raw data
|  +- manual/              #   Data processed manually
|  +- processed/           #   Data files processed automatically by the pipeline
+- scripts/                # Scripts for data processing
+- analyses/               # Analyses for the paper
|  +- 00-XXX.R             #   Scripts or functions (in order).
|  +- YYYY-MM-DD-.../      #   Preliminary versions of analyses.
+- requirements.txt        # Requirements for python virtual environment
+- README.md               # This file
+- Snakefile               # Top hierarchy of rules to process data for analysis
+- *.snakefile             # Specific rules to process data for analysis broken up by section
+- environment.yml         # YAML file to create Conda environment
```
