
# Parse GFFs

rule gff_to_tabs:
    input: ancient("data/processed/parsed-gffs/divided-{gfftype}-gffs")
    output: directory("data/processed/parsed-gffs/tabs/{gfftype}")
    threads: 4
    run:
        shell("mkdir -p {output}")
        cmd = " ".join([\
                          "python scripts/gff-to-tabular-parallel.py",
                          "-o",
                          "-s",
                          "-j",
                          format(threads),
                          "-D",
                          config["gffs"][wildcards.gfftype]["gfftabflags"]
                        ])

        shell("{cmd} -l {input} -d {output}")

rule all_gff_tabs:
    input: expand("data/processed/parsed-gffs/tabs/{gfftype}", \
                  gfftype=GFFTYPES)


# Make MIDAS

rule midas_gid_tsv:
    input: "data/raw/midas/midas_db_v1.0/"
    output: "data/processed/midas-to-gid.tsv"
    run:
        shell("python scripts/direc-to-gID.py -m {input} > {output}")

rule make_genome_divs:
    input: "data/processed/parsed-gffs/divided-{gfftype}-gffs"
    output: "data/processed/parsed-gffs/divided-{gfftype}-genomes"
    run:
        shell("cat {input} | scripts/gfflist-to-genomelist.py > {output}")

rule get_ncbi_dmp:
    output: "data/raw/names.dmp"
    shell:
        """
        cd data/raw
        wget https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
        tar xvf taxdump.tar.gz
        cd ../..
        """

rule process_results:
    input:
        tabs="data/processed/parsed-gffs/tabs/{gfftype}",
        hmmout="data/processed/hmm-output/{gfftype}",
        hmmtouch="data/processed/hmm-output/{gfftype}/.touch",
        dmp=ancient("data/raw/names.dmp"),
        div="data/processed/parsed-gffs/divided-{gfftype}-genomes"
    output:
        "data/processed/hmm-summaries/{gfftype}/processed-output-cut-{eval}.tab"
    run:
        is_mags = config["gffs"][wildcards.gfftype]["mags"]
        mag_flag = "--mags" if is_mags else "--no-mags"
        process_flags = config["gffs"][wildcards.gfftype]["processflags"]
        neighborhood = config["gffs"][wildcards.gfftype]["neighborhood"]
        filterexact = config["gffs"][wildcards.gfftype]["filterexact"]
        hmmout = os.path.dirname(input.hmmtouch)
        shell("python scripts/process_output_v2.py " +
              "--resultdir={hmmout} " +
              "--neighborhood={neighborhood} " +
              "--filterexact={filterexact} " +
              "--ethresh={wildcards.eval} " +
              "--divmapfile=" +
              "data/processed/parsed-gffs/divided-{wildcards.gfftype}-genomes " +
              "--namesfile={input.dmp} " +
              "--tabsdir=data/processed/parsed-gffs/tabs/{wildcards.gfftype}/ " +
              "--datadir=data/raw/ " +
              "--divided=True " +
              "--pidregex={is_mags} " +
              process_flags + " " + 
              "{mag_flag} > {output}")

rule process_all_results:
    input:
        expand( \
          "data/processed/hmm-summaries/{gfftype}/" +  
          "processed-output-cut-1e-10.tab", \
          gfftype=GFFTYPES)

rule make_burst_dbs:
    input: 'data/raw/phyeco.fa'
    output:
        edx='data/processed/burstdb/phyeco.edx',
        acx='data/processed/burstdb/phyeco.acx'
    threads: 4    
    run:
        shell("burst12 --threads 4 -r {input} -a {output.acx} -o {output.edx} -d QUICK 3000 -i 0.95")

rule midas_map_refseq:
    input:
        edx='data/processed/burstdb/phyeco.edx',
        acx='data/processed/burstdb/phyeco.acx'
    output: 'data/processed/midas_map_refseq/{subdir}/{name_of_genome(genome)}.matches.txt'
    threads: 4
    run:
        d = os.path.dirname(str(output))
        os.makedirs(d, exist_ok=True)
        (tfh, tf) = tempfile.mkstemp()
        if os.path.exists(genome):
            shell("zcat {genome} | scripts/make-2line.py | awk '{{print $1;}}' > {tf}")
            shell("burst12 --threads {threads} -m FORAGE -q {tf} -r {input.edx} -a {input.acx} -o {output} -i 0.95")
        else:
            print("(not found, skipping)")
            touch(outfile)

rule midas_map_all_refseq:
    input:
        drc='data/processed/parsed-gffs/divided-refseq-cds',
        edx='data/processed/burstdb/phyeco.edx',
        acx='data/processed/burstdb/phyeco.acx'
    output: 'data/processed/midas_map_refseq/{genome}.matches.txt'
    threads: 4
    run:
        genomeF = DividedCDSDict[wildcards.genome]
        d = os.path.dirname(str(output))
        os.makedirs(d, exist_ok=True)
        (tfh, tf) = tempfile.mkstemp()
        if os.path.exists(genomeF):
            shell("zcat {genomeF} | scripts/make-2line.py | awk '{{print $1;}}' > {tf}")
            shell("burst12 --threads {threads} -m FORAGE -q {tf} -r {input.edx} -a {input.acx} -o {output} -i 0.95")
        else:
            print("(not found, skipping)")
            shell("touch {output}")


def get_all_mmar(wc):
    return(expand("data/processed/midas_map_refseq/{genome}.matches.txt", \
        genome=DividedCDSDict.keys())) 

def make_midas_outf(row):
    return(os.path.join('data/processed/midas_map_refseq/',
                        row[0],
                        name_of_genome(row[1]) + ".matches.txt"))

rule mmar_all:
    input: get_all_mmar


def parse_divided(cdsfile):
    with open(cdsfile, 'r') as fh:
        outfiles = [make_midas_outf(line[:-1].split('\t')) for line in fh]
    return(outfiles)

rule summarize_midas_mapping:
    input: 'data/processed/midas_map_refseq/'
    output: 'data/processed/midas_map_refseq/summary.txt'
    run:
        shell("find {input} -name '*matches.txt' -exec scripts/midas_consensus.py {{}} \; > {output}")

rule phyeco_scan:
    input:
        genomelist='data/processed/parsed-gffs/list-refseq-genomes.txt',
        markerhmm='data/raw/phyeco.hmm'
    output: directory('data/processed/phyeco-scan/')
    run:
        shell("python scripts/phyeco_scanner.py -g {input.genomelist} -p {input.markerhmm} -o {output} -b -c 1e-20 -v 1")

def get_phyeco_inputs():
    ID, = glob_wildcards('data/processed/phyeco-scan/{ID,B[0-9]*}')
    return(ID)

def phyeco_combine_inputs(wc):
    IDS = get_phyeco_inputs()
    I = expand('data/processed/phyeco-scan/aln/{ID}-trim.aln', ID=IDS)
    return(I)

def phyeco_process_inputs(wc):
    IDS = get_phyeco_inputs()
    I = expand('data/processed/phyeco-scan/{ID}-filtered.fa', ID=IDS)
    return(I)

rule phyeco_process:
    input:
       phyeco='data/processed/phyeco-scan/{marker}',
       filter_list='data/manual/bacteria_plus_outgroup_refseq_accessions.txt'
    output:
       'data/processed/phyeco-scan/{marker,B0.*}-filtered.fa'
    run:
       shell("python scripts/rename_fasta_preTA.py -i {input.phyeco} -o {output} -g -F {input.filter_list} -d")

rule all_phyeco_process:
    input: phyeco_process_inputs

rule phyeco_align:
    input: 'data/processed/phyeco-scan/{marker}-filtered.fa'
    output: 'data/processed/phyeco-scan/aln/{marker,B[0-9]*}.aln'
    threads: 4
    run:
       shell("clustalo -i {input} -o {output} --threads={threads}")

rule phyeco_trim:
    input: 'data/processed/phyeco-scan/aln/{marker}.aln'
    output: 'data/processed/phyeco-scan/aln/{marker,B[0-9]*}-trim.aln'
    run:
        shell("AMAS.py trim -c {threads} -f fasta -i {input} -o {output} -u fasta -d aa -t 0.6")

rule phyeco_combine:
    # input: glob.glob('data/processed/phyeco-scan/aln/*-trim.aln')
    input: phyeco_combine_inputs
    output:
        aln='data/processed/phyeco-scan/aln/combined.aln',
        partition='data/processed/phyeco-scan/aln/partition.txt'
    run:
        shell("AMAS.py concat -c {threads} -u fasta -t {output.aln} -p {output.partition} -f fasta -d aa -i {input}")

# NOTE: Run this on Bueno!
rule phyeco_tree:
    input: aln='data/processed/phyeco-scan/aln/combined.aln'
    output: 'data/processed/phyeco-scan/aln/phyeco.treefile'
    threads: 4
    run:
        inputfile = os.path.basename(str(input))
        inputdir = os.path.abspath(os.path.dirname(str(input)))
        outputfile = os.path.basename(str(output))
        outputdir = os.path.abspath(os.path.dirname(str(output)))
        shell("cd {inputdir}; iqtree -s {inputfile} -nt AUTO -ntmax {threads} -m TEST -fast -pre phyeco")

rule phyeco_fasttree:
    input: aln='data/processed/phyeco-scan/aln/combined.aln'
    output: 'data/processed/phyeco-scan/aln/combined.FastTree'
    threads: 4
    run:
        inputfile = os.path.basename(str(input))
        inputdir = os.path.abspath(os.path.dirname(str(input)))
        outputfile = os.path.basename(str(output))
        outputdir = os.path.abspath(os.path.dirname(str(output)))
        shell("OMP_NUM_THREADS={threads} FastTreeMP {input} > {output}")
